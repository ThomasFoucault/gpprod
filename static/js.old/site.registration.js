/**
 * Gestion de l'upload
 */
function initUpload() {
	
	var $ = jQuery;
	var uploadBlocks = $('div.upload-file');
	
	var 
		forms = {},
		inputs = {},
		iframes = {},
		body = $('body'),
		loaded = {};
		
	var build = function(uploader) {	
		var name = uploader.attr('data-name'),
			button = uploader.find('label');
		var form = $('<form>', {
			action: uploader.attr('data-url')+'?key='+name,
			target: 'upload-frame-'+name,
			enctype: 'multipart/form-data',
			method: 'post'
		}).hide();
		body.append(form);
		
		var input = $('<input>', {
			type: 'file',
			name: name,
			id: name
		});
		form.append(input);
		
		var iframe = $('<iframe>', {
			src: 'javascript:false',
			id: 'upload-frame-'+name,
			name: 'upload-frame-'+name		
		}).hide();
		body.append(iframe);
		
		iframes[name] = iframe;		
		loaded[name] = false;		
		var currentIframe = iframes[name];		
		
		currentIframe.on('load', function(event) {			
			iframeLoaded(document.getElementById($(this).attr('id')), name);			
			loaded[name] = true;
		});
		
		forms[name] = form;
		inputs[name] = input;
		
		inputs[name].on('change', function() {
			loaded[name] = true;			
			button.addClass('loading');
			$('#preview-'+name).addClass('loading');
			$('#preview-'+name).attr('style', "width: 200px; height: 150px;");
			forms[name].submit();
		});
	};
	
	var iframeLoaded = function(currentIframe, name) {
		
		var document, response;
		document = currentIframe.contentDocument || currentIframe.contentWindow.document;		
		response = document.body.innerHTML;			
		if(!loaded[name]) {
            return;
        }
		
		$('#upload-file-'+name+' label, #preview-'+name).removeClass('loading');
		
		// Exécuté une fois par défaut
		if(response != 'false') {
			response = jQuery.parseJSON(response) // JSON.decode(response);
			
			if(response.success == true) {
				$('input[name=file-'+name+']').attr('value', response.files['final']);
				$('#preview-'+name).addClass('valid');
				var file = response.files['final'];
				var extension = file.substr( (file.lastIndexOf('.') +1) );
				switch(extension) {
					case 'jpg':
					case 'jpeg':
					case 'png':
					case 'gif':
						$('#preview-'+name).attr('style', "width: 200px; height: 150px; background:#F9F8F4 url('"+response.files['path']+'/'+file+"') center center / 100% auto");
					break; 
				}
			}else{				
				var errorMessage = '';
				jQuery.each(response.errors, function(indexError, error) {
					errorMessage += error+"\n";
				});
				if(errorMessage == '') {
					errorMessage = 'Une erreur s\'est produite.';
				}
				alert(errorMessage);
			}			
			valid_form_7();
		} else {
			alert('Une erreur s\'est produite.');
			console.log(response.errors);
		}
	};
	
	$.each(uploadBlocks, function(index, uploadBlock){
		build($(uploadBlock));
	});
}

function isFlash(){
	if( navigator.mimeTypes.length > 0){
		return navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin != null;
	}else if( window.ActiveXObject ){
		try{
			new ActiveXObject( "ShockwaveFlash.ShockwaveFlash" );
			return true;
		}catch( oError ){
			return false;
		}
	}else{
		return false;
	}
}

function isTablet() {
	/*try {
		return (window.matchMedia('(max-width: 1200px)').matches);
	} catch(error) {
		return false;
	}*/
	return (support == 'tablet' || support == 'phone');
}

function isMobile() {
	return (support == 'phone');
}

function verifDate(a,m,j){ 
	/*console.log(a)
	console.log(m)
	console.log(j)*/
	m-=1;
	d = new Date(a,m,j);
	return (d.getFullYear()!=a || d.getMonth()!=m) ? false : true;
}

function verifEmail(email) {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	return (email == '' || !reg.test(email));
}

function verifText(text){
	var reg = /^\D+$/;
	return (text == '' || !reg.test(text));
}

/**
 * Retourne si une chaine de caractères correspond à une année
 */
function isYear(year) {
	return /^[0-9]{4}$/.test(year);
}

function verifPhone(phone){
	var numbers = new String(phone);

	numbers = numbers.replace(/[^0-9]/g, '');
	
	
	if( numbers == "" ) return false;
	nb = numbers.length;

	
	if (nb!=10) return false;
	
	return true
}

function verifIBAN(iban){
	var ibanValidationModulo = 97; // On utilise var au lieu de const, qui présente des incompatibilités sur IE8
 
    // On force les caractères alphabétiques en majuscule
    iban = iban.toUpperCase();
    // on supprime les espaces
    iban = iban.replace(new RegExp(" ", "g"), "");
 
    // le code iban doit faire plus de 14 caractères
    if (iban.length < 15) {
        return false;
    }
 
    // puis on transfert les quatre premiers caractères en fin de chaine.
    modifiedIban = iban.substring(4, iban.length)+iban.substr(0,4);
 
    // On convertit les caractères alphabétiques en valeur numérique
    numericIbanString = "";
    for (var index = 0; index < modifiedIban.length; index ++) {
        currentChar = modifiedIban.charAt(index);
        currentCharCode = modifiedIban.charCodeAt(index);
 
        // si le caractère est un digit, on le recopie
        if ((currentCharCode > 47) && (currentCharCode <  58)) {
            numericIbanString = numericIbanString + currentChar;
        }
        // si le caractère est une lettre, on le converti en valeur
        else if ((currentCharCode > 64) && (currentCharCode < 91)) {
            value = currentCharCode-65+10;
            numericIbanString = numericIbanString + value;
        }
        // sinon, le code iban est invalide (caractère invalide).
        else {
            return false;
        }
    }
 
    // On a maintenant le code iban converti en un nombre. Un très gros nombre.
    // Tellement gros que javascript ne peut pas le gérer.
    // Pour calculer le modulo, il faut donc y aller par étapes :
    // on découpe le nombre en blocs de 5 chiffres.
    // Pour chaque bloc, on préfixe avec le modulo du bloc précédent (2 chiffres max,
    // ce qui nous fait un nombre de 7 chiffres max, gérable par javascript).
    var previousModulo = 0;
    for (var index = 0; index < numericIbanString.length; index += 5) {
        subpart = previousModulo+""+numericIbanString.substr(index, 5);
        previousModulo = subpart % ibanValidationModulo;
    }
 
    return previousModulo == 1;
}

function verifPotable(phone){
	var numbers = new String(phone);

	numbers = numbers.replace(/[^0-9]/g, '');
	
	if( numbers == "" ) return false;
	nb = numbers.length;

	if (nb!=10) return false;
	
	if(numbers.substring(0,2) != '06' && numbers.substring(0,2) != '07') return false;
	
	return true
}

function verifNumber(number){
	number = number.replace(/ /g,"");
	return !isNaN(parseFloat(number)) && isFinite(number);
}

function resize_sidebar(){
	var height = jQuery('#sidebar').height() - jQuery('#sidebar_pres').height() - 40;
	height = height / 7;
	jQuery('#sidebar ul#sidebar_steps li').attr('style', 'line-height:'+height+'px');
	height = height - 35;
	height = height / 2;
	jQuery('#sidebar ul#sidebar_steps li .sidebar_number').attr('style', 'top:'+height+'px');
	
	if(jQuery('body').width() > 768){
		jQuery('#sidebar_client').show();
	}else{
		jQuery('#sidebar_client').hide();
	}
}

function scrollTo(id){
	jQuery('html, body').animate({
		scrollTop: jQuery("#"+id).offset().top
	}, 500);
}

function number_format(number, decimals, dec_point, thousands_sep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function(n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k)
		.toFixed(prec);
	};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

function reset_data($){
	var ids = accounts.split(',');
	var position = 0;
	var perf = 0;
	var perfp = 0;
	var ar = 0;
	var apports = 0;
	var retraits = 0;
	var frais = 0;
	var index;
	
	for(index = 0; index < ids.length; index++){
		if(ids[index] != '0' && ids[index] != 0){
			position+= parseFloat($('#position_'+ids[index]).val());
			perf+= parseFloat($('#perfeur_'+filter+'_'+ids[index]).val());
			perfp+= parseFloat($('#perf_'+filter+'_'+ids[index]).val());
			apports+= parseFloat($('#apport_'+filter+'_'+ids[index]).val());
			retraits+= parseFloat($('#retrait_'+filter+'_'+ids[index]).val());
			frais+= parseFloat($('#frais_'+filter+'_'+ids[index]).val());
			ar+= parseFloat($('#ar_'+filter+'_'+ids[index]).val());
		}		
	}
	
	$('#synthesis_position > .full_number').html(number_format(position, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_apports > .full_number').html(number_format(apports, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_retraits > .full_number').html(number_format(retraits, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_frais > .full_number').html(number_format(frais, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_perf > .full_number').html(number_format(perf, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_ar > .full_number').html(number_format(ar, 0, ',', ' ')+' <span class="decimal">€</span>');
	
	$('.account_perf_pos').attr('data-original-title', 'SOMME DE VOS POSITIONS<br>La somme de votre/vos compte(s) séléctionné(s) au '+date+' est de '+number_format(position, 0, ',', ' ')+' <span class="decimal">€</span><br><br>PERFOMANCE DE VOS ACTIFS<br>Au cours de la période, la performance de votre/vos compte(s) séléctionné(s) est de '+number_format(perf, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_apports').attr('data-original-title', 'APPORTS EFFECTUÉS<br>Au cours de la période, la somme des apports effectués sur votre/vos compte(s) sélectionné(s) est de '+number_format(apports, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_retraits').attr('data-original-title', 'RETRAITS EFFECTUÉS<br>Au cours de la période, la somme des retraits effectués sur votre/vos compte(s) sélectionné(s) est de '+number_format(retraits, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_frais').attr('data-original-title', 'FRAIS PRÉLEVÉS<br>Au cours de la période, la somme des frais prélevés sur votre/vos compte(s) sélectionné(s) est de '+number_format(frais, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_ar').attr('data-original-title', 'APPORTS / RETRAITS<br>Au cours de la période, la somme des apports/retraits effectués sur votre/vos compte(s) sélectionné(s) est de '+number_format(ar, 0, ',', ' ')+' <span class="decimal">€</span>');
	
	$('#synthesis_perf').removeClass('negative');
	if(perf <0){
		$('#synthesis_perf').addClass('negative');
	}
	
	/*perfp = number_format(perfp, 2, ',', ' ');
	perfp = perfp.split(',');
	perfp = perfp[0] + ',<span class="decimal">' + perfp[1] + '</span> <span class="decimal">%</span>';
	$('#synthesis_perf .percent > .full_number').html(perfp);*/
}

var activity = false;
var sidebarClient = false;
var sidebarClientHideClick = false;


jQuery(document).ready(function($){	
	initUpload();
	
	$('#toggle_sidebar').click(function(){
		if(!sidebarClient && !sidebarClientHideClick){
			$( "#sidebar_client").slideDown();
			sidebarClient = true;
		}else{
			$( "#sidebar_client").slideUp();
			sidebarClient = false;
			sidebarClientHideClick = false;
		}
	});	
	
	$(document).mouseup(function (e){
		var container = $("#activity_block_container");
		if(container.has(e.target).length === 0 && activity){
			$("#activity_block").animate({width:"0px"}, 500, function(){activity = false;});
			$("#activity_block_container").width("0px");
		}
		
		var container = $("#sidebar_client");
		var button = $("#toggle_sidebar");
		if(container.has(e.target).length === 0 && button.has(e.target).length === 0 && jQuery('body').width() <= 768 && sidebarClient){
			container.hide();
			sidebarClient = false;
			sidebarClientHideClick = true;
			setTimeout(function(){sidebarClientHideClick = false;}, 500);
		}
	});
	
	$('.tooltip-input').tooltip({'placement':'top', 'trigger':'focus', 'html':true});
	
	$('#preview-rib').click(function(){
		var cb = document.getElementById("label-rib");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-identity').click(function(){
		var cb = document.getElementById("label-identity");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-address').click(function(){
		var cb = document.getElementById("label-address");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-bank').click(function(){
		var cb = document.getElementById("label-bank");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-titres').click(function(){
		var cb = document.getElementById("label-titres");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-pea').click(function(){
		var cb = document.getElementById("label-pea");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-peapme').click(function(){
		var cb = document.getElementById("label-peapme");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-identity_pass').click(function(){
		var cb = document.getElementById("label-identity_pass");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-identity_card_r').click(function(){
		var cb = document.getElementById("label-identity_card_r");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-identity_card_v').click(function(){
		var cb = document.getElementById("label-identity_card_v");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-identity_visa_r').click(function(){
		var cb = document.getElementById("label-identity_visa_r");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	$('#preview-identity_visa_v').click(function(){
		var cb = document.getElementById("label-identity_visa_v");
		if (cb.dispatchEvent) {
			var evt = document.createEvent("MouseEvents");
			evt.initEvent("click", true, true);
			cb.dispatchEvent(evt);
		} else {
			cb.click();
		}
	});
	
	// Ajout d'un loader sur le bouton de soumission
	$('form').on('submit', function() {
		$(this).find('input[type=submit]').addClass('loading');
	});
	
	$('a.mouv_load_more').click(function(){
		mouv_block_3 = mouv_block_3 + 1;
		$('table#mouvements tr.mouv_block_'+mouv_block_3+', table#mouvements_mobile tr.mouv_block_'+mouv_block_3).show();
		if(mouv_block_3 == max_mouv_block_3){
			$('.mouv_load_more').hide();
		}
		/*switch($(this).attr('rel')){
			case '0':
				mouv_block_0 = mouv_block_0 + 1;
				$('.filter_0 table#mouvements tr.mouv_block_'+mouv_block_0+', .filter_0 table#mouvements_mobile tr.mouv_block_'+mouv_block_0).show();
				if(mouv_block_0 == max_mouv_block_0){
					$('.filter_0 .mouv_load_more').hide();
				}
				break;
			case '1':
				mouv_block_1 = mouv_block_1 + 1;
				$('.filter_1 table#mouvements tr.mouv_block_'+mouv_block_1+', .filter_1 table#mouvements_mobile tr.mouv_block_'+mouv_block_1).show();
				if(mouv_block_1 == max_mouv_block_1){
					$('.filter_1 .mouv_load_more').hide();
				}
				break;
			case '2':
				mouv_block_2 = mouv_block_2 + 1;
				$('.filter_2 table#mouvements tr.mouv_block_'+mouv_block_2+', .filter_2 table#mouvements_mobile tr.mouv_block_'+mouv_block_2).show();
				if(mouv_block_2 == max_mouv_block_2){
					$('.filter_2 .mouv_load_more').hide();
				}
				break;
			case '3':
				mouv_block_3 = mouv_block_3 + 1;
				$('.filter_3 table#mouvements tr.mouv_block_'+mouv_block_3+', .filter_3 table#mouvements_mobile tr.mouv_block_'+mouv_block_3).show();
				if(mouv_block_3 == max_mouv_block_3){
					$('.filter_3 .mouv_load_more').hide();
				}
				break;
		}*/
	});
	
	
	$('.tooltip_top').tooltip({'placement':'top', 'html':true});
	$('.tooltip_bottom, .account_perf_pos, .account_synthesis > .account_block_number').tooltip({'placement':'bottom', 'html':true});	
	$('input, textarea').placeholder();
	
	$('a#message_link').click(function(){
		$("#activity_block").animate({width:"300px"}, 500, function(){activity = true;});
		$("#activity_block_container").width("400px");
		if($(this).hasClass('unread')){
			$(this).removeClass('unread');
			$(this).addClass('read');
			$(this).html('');
			
			$.ajax({
				url: base_url+"activities/read",
				type: 'POST',
				success: function(msg){}
			}); 
		}
	});
	
	$('a#close_activity_block').click(function(){
		$("#activity_block").animate({width:"0px"}, 500, function(){activity = false;});
		$("#activity_block_container").width("0px");
	});
	
	$('a#close_sidebar_block').click(function(){
		$( "#sidebar_client" ).slideUp();
	});
		
	if($('#breadcrump-nav-selection').length){
		$('#breadcrump-nav li').click(function(){
			if($(this).attr('id') != 'mother-section'){
				scrollTo($(this).attr('target'));
			}
		});
		
		var selected_menu = $('#breadcrump-nav li[target=breadcrump_target_1]');
		var breadcrump_arrow = $('#breadcrump-nav-selection');
		var left = selected_menu.position().left + (selected_menu.width() / 2);
		breadcrump_arrow.css('left', left);
		
		$(window).scroll(function(){
			posScroll = $(document).scrollTop();
			
			var previous = $('#breadcrump_target_1');
			$('.breadcrump_target').each(function(){
				if(posScroll >= ($(this).position().top - 30)){
					previous = $(this);
				}else{
					return false;
				}
			});
			
			var current_menu = $('#breadcrump-nav li[target='+previous.attr('id')+']');
			if(current_menu.length){
				if(selected_menu.position().left != current_menu.position().left){
					var left = current_menu.position().left + (current_menu.width() / 2);
					breadcrump_arrow.animate({left:left}, 100);
					selected_menu = current_menu;
				}
			}
		});
	}
	
	$('input[name=graph_period]').change(function(){
		$('.filter_0, .filter_1, .filter_2, .filter_3').hide();
		$('.filter_'+$(this).val()).show();
		$('.account_block_number.filter_'+$(this).val()).attr('style', 'display:inline-block;');
		filter = $(this).val();
		load_data();
		/*if($('#movements_wrapper').length){
			$.ajax({
				url: full_url+"account/movements/"+account_number,
				type: 'POST',
				data: 'filter='+filter,
				success: function(msg){
					$('#movements_wrapper').html(msg);
				}
			});
		}*/
		if($('input[name=graph_period2]').length){
			switch($(this).val()){
				case '0':
					$('#graph_period22, #graph_period23, #graph_period24').removeAttr('checked', 'checked');
					$('#graph_period21').attr('checked', 'checked');
					break;
				case '1':
					$('#graph_period21, #graph_period23, #graph_period24').removeAttr('checked', 'checked');
					$('#graph_period22').attr('checked', 'checked');
					break;
				case '2':
					$('#graph_period22, #graph_period21, #graph_period24').removeAttr('checked', 'checked');
					$('#graph_period23').attr('checked', 'checked');
					break;
				case '3':
					$('#graph_period22, #graph_period23, #graph_period21').removeAttr('checked', 'checked');
					$('#graph_period24').attr('checked', 'checked');
					break;
			}
		}
		
		if($('.desktop_account').length){
			reset_data($);
		}
	});
	
	$('input[name=graph_period2]').change(function(){
		$('.filter_0, .filter_1, .filter_2, .filter_3').hide();
		$('.filter_'+$(this).val()).show();
		$('.account_block_number.filter_'+$(this).val()).attr('style', 'display:inline-block;');
		filter = $(this).val();
		load_data();
		
		switch($(this).val()){
			case '0':
				$('#graph_period2, #graph_period3, #graph_period4').removeAttr('checked', 'checked');
				$('#graph_period1').attr('checked', 'checked');
				break;
			case '1':
				$('#graph_period1, #graph_period3, #graph_period4').removeAttr('checked', 'checked');
				$('#graph_period2').attr('checked', 'checked');
				break;
			case '2':
				$('#graph_period2, #graph_period1, #graph_period4').removeAttr('checked', 'checked');
				$('#graph_period3').attr('checked', 'checked');
				break;
			case '3':
				$('#graph_period2, #graph_period3, #graph_period1').removeAttr('checked', 'checked');
				$('#graph_period4').attr('checked', 'checked');
				break;
		}
	});
	
	/*******************************************
	 *               Formulaire                *
	 *******************************************/
	
	$('input[type=submit], #sidebar_steps a').click(function(){
		$('div#loading_overall').show();
	});
	
	var refreshAddPhone = function() {
		
		var countPhones = 0
		$('fieldset.fieldset-phone div.form-group').each(function() {
			var phone = $(this);
			if(phone.attr('id') != 'add_phone' && phone.css('display') == 'block') {
				countPhones++;
			}
		});
		if(countPhones == 4) {
			$('#add_phone').hide();
		}
		
	};
	
	refreshAddPhone();
	resize_sidebar();
	
	if($('#funds_iframe').length){
		if($('body').width() > 768){
			$('#funds_iframe').attr('style', 'padding-left:220px');
		}else{
			$('#funds_iframe').attr('style', 'padding-left:0');
		}
	}
	
	if($('.filter_gp').length){
		if($('body').width() > 768){
			$('.filter_gp .segmented-control').addClass('medium_size');
			$('.filter_gp .segmented-control').removeClass('full_size');
		}else{
			$('.filter_gp .segmented-control').removeClass('medium_size');
			$('.filter_gp .segmented-control').addClass('full_size');
		}
	}
	
	if($('#account_top_right').length){
		if($('body').width() > 1200){
			$('#account_top_right .segmented-control').addClass('medium_size');
			$('#account_top_right .segmented-control').removeClass('full_size');
		}else{
			$('#account_top_right .segmented-control').removeClass('medium_size');
			$('#account_top_right .segmented-control').addClass('full_size');
		}
	}
	
	$(window).on('resize', function(){
		resize_sidebar();
		
		if($('#funds_iframe').length){
			if($('body').width() > 768){
				$('#funds_iframe').attr('style', 'padding-left:220px');
			}else{
				$('#funds_iframe').attr('style', 'padding-left:0');
			}
		}
		
		if($('.filter_gp').length){
			if($('body').width() > 768){
				$('.filter_gp .segmented-control').addClass('medium_size');
				$('.filter_gp .segmented-control').removeClass('full_size');
			}else{
				$('.filter_gp .segmented-control').removeClass('medium_size');
				$('.filter_gp .segmented-control').addClass('full_size');
			}
		}
		
		if($('#account_top_right').length){
			if($('body').width() > 1200){
				$('#account_top_right .segmented-control').addClass('medium_size');
				$('#account_top_right .segmented-control').removeClass('full_size');
			}else{
				$('#account_top_right .segmented-control').removeClass('medium_size');
				$('#account_top_right .segmented-control').addClass('full_size');
			}
		}
	});
	
	$("input.control-number").autoNumeric('init', {aSep: ' ', mDec: '0'});
	//$("#phone_number, #phone_number_home, #phone_number_work, #phone_number_home2").mask("99 99 99 99 99");
	
	$('#add_phone a').click(function(){
		
		$('.form-group#phone_'+$(this).attr('rel')).show();
		refreshAddPhone();
		$(this).hide();
	});
	
	$('.form-control').focus(function(){
		$(this).parent().addClass('focus');
	});
	
	$('.form-control').blur(function(){
		$(this).parent().removeClass('focus');
	});
	
	$('input[name=civility]').change(function(){
		if($(this).val() == 'Madame'){
			$('#lastgirlname_content').show();
			madam = true;
		}else{
			$('#lastgirlname_content').hide();
			madam = false;
		}
	});
	
	$('input[name=PAT_IMMOB__c]').change(function(){
		if($(this).val() == '1'){
			$('#PAT_IMMOB__c_block').show();
		}else{
			$('#PAT_IMMOB__c_block').hide();
		}
	});
	
	$('input[name=identity_type]').change(function(){
		identity = $(this).val();
		if($(this).val() == 'CNI'){
			$('#identity_card_block').show();
			$('#identity_pass_block').hide();
			$('#identity_visa_block').hide();
		}else{
			if($(this).val() == 'Passeport'){
				$('#identity_card_block').hide();
				$('#identity_pass_block').show();
				$('#identity_visa_block').hide();
			}else{
				$('#identity_card_block').hide();
				$('#identity_pass_block').hide();
				$('#identity_visa_block').show();
			}
		}
	});
	
	$('input[name=account_subscribe]').change(function(){
		if($(this).val() == '1' || $(this).val() == 1){
			subscribe = true;
		}else{
			subscribe = false;
		}
	});
	
	$('input[name=account_subscribe2]').change(function(){
		if($(this).val() == '1' || $(this).val() == 1){
			subscribe2 = true;
		}else{
			subscribe2 = false;
		}
	});
	
	$('input[name=account_horizon]').change(function(){
		horizon = $(this).val();
		if($(this).val() == 'Court - moyen terme (> 2 ans)'){
			$('.medium_large').hide();
			$('.small_medium').show();
		}else{
			$('.small_medium').hide();
			$('.medium_large').show();
		}
	});
	
	$('input[name=account_subscribe_risk]').change(function(){
		risk = true;
		if($(this).val() == '1'){
			$('#account_subscribe_norisk2').removeAttr('checked', 'checked');
			$('#account_subscribe_norisk1').attr('checked', 'checked');
		}else{
			$('#account_subscribe_norisk1').removeAttr('checked', 'checked');
			$('#account_subscribe_norisk2').attr('checked', 'checked');
		}
	});
	
	$('input[name=account_subscribe_norisk]').change(function(){
		risk = true;
		if($(this).val() == '1'){
			$('#account_subscribe_risk2').removeAttr('checked', 'checked');
			$('#account_subscribe_risk1').attr('checked', 'checked');
		}else{
			$('#account_subscribe_risk1').removeAttr('checked', 'checked');
			$('#account_subscribe_risk2').attr('checked', 'checked');
		}
	});
	
	$('input[name=invest_pro]').change(function(){
		if($(this).val() == 'Oui'){
			$('#invest_pro_block').show();
		}else{
			$('#invest_pro_block').hide();
		}
	});
	
	$('#add_account').click(function(){
		$('#account_create_form, #create_another_account').show();
		$(this).hide();
	});
	
	$('#change_cni').click(function(){
		$('#identity_card_block > .row').show();
		$('#identity_card_ok').hide();
	});
	
	$('#change_visa').click(function(){
		$('#identity_visa_block > .row').show();
		$('#identity_visa_ok').hide();
	});
	
	$('select#country, select#country_fisc').change(function(){
		if($('#is_fiscal').val() == 0){
			eval("country_code = countries['"+$('select#country_fisc').val()+"'];");
		}else{
			eval("country_code = countries['"+$('select#country').val()+"'];");
		}
		
		if($('select[name=country]').val()=='États-Unis' || $('select[name=country_fisc]').val()=='États-Unis'){
			$('.us_error').show();
			
			jQuery('select[name=country], select[name=country_fisc]').removeClass('valid');
			jQuery('select[name=country], select[name=country_fisc]').parent().removeClass('valid');
			jQuery('select[name=country], select[name=country_fisc]').parent().parent().removeClass('valid');
			
			jQuery('select[name=country], select[name=country_fisc]').addClass('error');
			jQuery('select[name=country], select[name=country_fisc]').parent().addClass('error');
			jQuery('select[name=country], select[name=country_fisc]').parent().parent().addClass('error');
			if(jQuery('select[name=country]').val()!='États-Unis'){
				jQuery('#us_error_country_1').hide();
				
				jQuery('select[name=country]').removeClass('error');
				jQuery('select[name=country]').parent().removeClass('error');
				jQuery('select[name=country]').parent().parent().removeClass('error');
				
				jQuery('select[name=country]').addClass('valid');
				jQuery('select[name=country]').parent().addClass('valid');
				jQuery('select[name=country]').parent().parent().addClass('valid');
			}
			if(jQuery('select[name=country_fisc]').val()!='États-Unis'){
				jQuery('#us_error_country_2').hide();
				
				jQuery('select[name=country_fisc]').removeClass('error');
				jQuery('select[name=country_fisc]').parent().removeClass('error');
				jQuery('select[name=country_fisc]').parent().parent().removeClass('error');
				
				jQuery('select[name=country_fisc]').addClass('valid');
				jQuery('select[name=country_fisc]').parent().addClass('valid');
				jQuery('select[name=country_fisc]').parent().parent().addClass('valid');
			}
			$('#submit_grp').hide();
		}else{
			$('.us_error').hide();
			$('#submit_grp').show();
		}
	});
	
	$('input#phone_number, input#phone_number_home, input#phone_number_work, input#phone_number_home2').blur(function(){
		$(this).val(formatLocal(country_code, $(this).val()));
	});
	
	$('#relationship_origin').change(function(){
		if($(this).val() == 'Recommandation client'){
			$('#relationship_origin_block').show();
		}else{
			$('#relationship_origin_block').hide();
		}
	});
	
	$('input[name=demarche]').change(function(){
		if($(this).val() == '1'){
			$('#demarche_block').show();
			show_demarche = true;
		}else{
			$('#demarche_block').hide();
			show_demarche = false;
		}
	});
	
	$('input[name=political_function]').change(function(){
		if($(this).val() == 'Oui'){
			$('#political_function_block').show();
			show_politic = true;
		}else{
			$('#political_function_block').hide();
			show_politic = false;
		}
	});
	
	$('select[name=account_type]').change(function(){
		if($(this).val() != ''){
			$('#account_type_block').show();
			
			if($(this).val() == 'Compte Titres'){
				$('#account_horizon_block1').show();
				$('#account_horizon_block2').hide();
			}else{
				$('#account_horizon_block1').hide();
				$('#account_horizon_block2').show();
				$('#account_horizon2').attr('checked', 'checked');
				$('.small_medium').hide();
				$('.medium_large').show();
			}
		}else{
			$('#account_type_block').hide();
		}
	});
	
	$('select[name=account_objective]').change(function(){
		if($(this).val() == 'other'){
			$('#account_objective_block').show();
		}else{
			$('#account_objective_block').hide();
		}
	});
	
	$('select[name=account_origin]').change(function(){
		if($(this).val() == 'donate' || $(this).val() == 'legacy' || $(this).val() == 'sells' || $(this).val() == 'other'){
			$('#account_origin_block').show();
		}else{
			$('#account_origin_block').hide();
		}
	});
	
	$('#Heritage__c_content').click(function(){
		if($('#Heritage__c').val() == 1){
			$('#Heritage__c_block').show();
		}else{
			$('#Heritage__c_block').hide();
			$('#Heritage_Desc__c').val('');
		}
	});
	
	$('#Donation__c_content').click(function(){
		if($('#Donation__c').val() == 1){
			$('#Donation__c_block').show();
		}else{
			$('#Donation__c_block').hide();
			$('#Donation_Desc__c').val('');
		}
	});
	
	$('#OBJ_AUTRE__c_content').click(function(){
		if($('#OBJ_AUTRE__c').val() == 1){
			$('#account_objective_block').show();
		}else{
			$('#account_objective_block').hide();
			$('#account_objective_precision').val('');
		}
	});
	
	$('#Vente_immobilier__c_content').click(function(){
		if($('#Vente_immobilier__c').val() == 1){
			$('#Vente_immobilier__c_block').show();
		}else{
			$('#Vente_immobilier__c_block').hide();
			$('#Vente_immobilier_Desc__c').val('');
		}
	});
	
	$('#Autres__c_content').click(function(){
		if($('#Autres__c').val() == 1){
			$('#Autres__c_block').show();
		}else{
			$('#Autres__c_block').hide();
			$('#Autres_Desc__c').val('');
		}
	});
	
	$('select[name=account_method]').change(function(){
		if($(this).val() == 'transfert de titres'){
			$('.account_method_block').show();
			$('#sidebar').height($('#wrapper').height());
		}else{
			$('.account_method_block').hide();
			$('#sidebar').height($('#wrapper').height());
		}
	});
	
	$('#is_fiscal_content .form_check, #is_fiscal_content label').click(function(){
	
		
		if($('input[name=is_fiscal]').val() == 0){
			$('#other_address').show();
			show_fiscal = true;
		}else{
			$('#other_address').hide();
			show_fiscal = false;
		}
		
		valid_form_2();
	});
	
	$('#nature_pro_content .form_check, #nature_pro_content label').click(function(){
	
		/*var changeDisplay = function(object) {
		
		
			if(object.css('display') == 'none') {
				object.css('display', 'block');
			} else {
				object.css('display', 'none');
			}
		}*/
		
		//changeDisplay($('div#society_block'));
		
	
		if($('input[name=nature_pro]').val() == 1){
			$('#pro_block').show();
			show_pro = true;
		}else{
			$('#pro_block').hide();
			show_pro = false;
		}
	});
	
	$('#nature_others_content .form_check, #nature_others_content label').click(function(){
		if($('input[name=nature_others]').val() == 1){
			$('#other_block').show();
			show_other = true;
		}else{
			$('#other_block').hide();
			show_other = false;
		}
	});
	
	$('#nature_others_content .form_check, #nature_others_content label, #nature_pro_content .form_check, #nature_pro_content label, #nature_foncier_content .form_check, #nature_foncier_content label, #nature_retraite_content .form_check, #nature_retraite_content label').click(function(){
		valid_form_3();
		if($('#nature_pro').val()==0 && $('#nature_foncier').val()==0 && $('#nature_retraite').val()==0 && $('#nature_others').val()==0){
			$('#nature_error').show();
		}else{
			$('#nature_error').hide();
		}
	});
	
	$('.checboxes .check_content .form_check, .checboxes .check_content label').click(function(){
		var id = $(this).parent().find('input').attr('id');
		if($('input[name='+id+']').val() == 1){
			$('#'+id+'_block').show();
		}else{
			$('#'+id+'_block').hide();
		}
	});
	
	$('input, select').blur(function(){
		$(this).removeClass('error');
		$(this).removeClass('valid');
		$(this).parent().removeClass('error');
		$(this).parent().removeClass('valid');
		$(this).parent().parent().removeClass('error');
		$(this).parent().parent().removeClass('valid');
		$('#pea_amount_error, #peapme_amount_error').hide();
		
		var correct = true;
		
		if($(this).hasClass('required') && $(this).val()==''){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			if($(this).hasClass('form-select')){
				$(this).parent().parent().addClass('error');
			}
			correct = false;
		}
		
		if($(this).hasClass('control-text') && $(this).val().length < 2 && $(this).val().length > 0){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-textonly') && $(this).val().length < 2 && $(this).val().length > 0){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}else{
			if($(this).hasClass('control-textonly') && verifText($(this).val())){
				$(this).addClass('error');
			$(this).parent().addClass('error');
				correct = false;
			}			
		}
		
		if($(this).hasClass('control-email') && $(this).val()!='' && verifEmail($(this).val())){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-phone') && $(this).val()!='' && !isValidNumber($(this).val(), country_code)){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-portable') && $(this).val()!='' && !isValidNumber($(this).val(), country_code)){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-number') && $(this).val()!='' && !verifNumber($(this).val())){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-iban') && $(this).val()!='' && !verifIBAN($(this).val())){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-bic') && $(this).val()!='' && $(this).val().length > 11){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($('form#step_form6').hasClass('step_form') && $(this).attr('id')=='account_amount'){
			var accountType = $('select#account_type').val();
			var value = $(this).val();
			value = value.replace(/ /g,"");
			value = parseFloat(value);
			if((accountType == 'Compte PEA' && value > 152000) || (accountType == 'Compte PEA-PME' && value > 75000)) {
				$(this).addClass('error');
				$(this).parent().addClass('error');
				correct = false;
				
				if(accountType == 'Compte PEA'){
					$('#pea_amount_error').show();
				}
				
				if(accountType == 'Compte PEA-PME'){
					$('#peapme_amount_error').show();
				}
			}
		}
		
		if($(this).attr('id') == 'birthcountry' || $(this).attr('id') == 'country' || $(this).attr('id') == 'country_fisc'){
			if($(this).val() == 'États-Unis'){
				correct = false;
				$(this).addClass('error');
				$(this).parent().addClass('error');
				$(this).parent().parent().addClass('error');
			}
		}
		
		if(correct){
			$(this).addClass('valid');
			$(this).parent().addClass('valid');
			if($(this).hasClass('control-phone') || $(this).hasClass('control-portable') || $(this).hasClass('control-number')){
				$(this).parent().parent().addClass('valid');
			}
			if($(this).hasClass('form-select')){
				$(this).parent().parent().addClass('valid');
			}
		}
	});
	
	/**
	 * Recalcul des valeurs actions, obligations et monétaires sur le formulaire de partimoine (étape 4)
	 */
	$('div#percent_action_content, div#percent_oblig_content, div#percent_monet_content').on('slidechange', function() {
		
		// Initialisation des variables utilisés
		var currentSlide = $(this), currentId = currentSlide.attr('id'),
			actionsValue = 0, obligationsValue = 0, monetaryValue = 0,
			newMonetaryValue = 0, newObligationsValue = 0, newActionsValue = 0,
		
		// On initialise les valeurs actuelles
			actions = ((currentId == 'percent_action_content') ? currentSlide : $('div#percent_action_content')),
			obligations = ((currentId == 'percent_oblig_content') ? currentSlide : $('div#percent_oblig_content')),
			monetary = ((currentId == 'percent_monet_content') ? currentSlide : $('div#percent_monet_content')),
		
		// Raffraichit les valeurs des variables
			getValues = function() {
				actionsValue = actions.slider('option', 'value');
				obligationsValue = obligations.slider('option', 'value');
				monetaryValue = monetary.slider('option', 'value');
			},
		
		// Calcul du monétaire
			processMonetary = function() {
				getValues();
				newMonetaryValue = 100 - (actionsValue + obligationsValue);
				newMonetaryValue = (newMonetaryValue < 0) ? 0 : newMonetaryValue;
				if(newMonetaryValue != monetaryValue) {
					monetaryValue = newMonetaryValue;
					$('div#percent_monet_content').slider('option', 'value', monetaryValue);
					$('span#percent_monet_number').html(monetaryValue + '%');
					$('input#percent_monet').val(monetaryValue);
				}
			},
		
		// Calcul des obligations
			processObligations = function() {
				getValues();
				newObligationsValue = 100 - (actionsValue + monetaryValue);
				newObligationsValue = (newObligationsValue < 0) ? 0 : newObligationsValue;
				if(newObligationsValue != obligationsValue) {
					obligationsValue = newObligationsValue;
					$('div#percent_oblig_content').slider('option', 'value', obligationsValue);
					$('span#percent_oblig_number').html(obligationsValue + '%');
					$('input#percent_oblig').val(obligationsValue);
				}
			},
		
		// Calcul des actions
			processActions = function() {
				getValues();
				newActionsValue = 100 - (obligationsValue + monetaryValue);
				newActionsValue = (newActionsValue < 0) ? 0 : newActionsValue;
				if(newActionsValue != actionsValue) {
					actionsValue = newActionsValue;
					$('div#percent_action_content').slider('option', 'value', actionsValue);
					$('span#percent_action_number').html(actionsValue + '%');
					$('input#percent_action').val(actionsValue);
				}
			}
		;
		
		// On adapte à 100 % les autres valeurs
		if(currentId == 'percent_action_content') {			// On a modifié les actions
			// On traite d'abord le monétaire
			processMonetary();
			// Puis l'obligation
			processObligations();
			
		} else if(currentId == 'percent_oblig_content') {	// On a modifié les obligations
			// On traite d'abord le monétaire
			processMonetary();
			// Puis les actions
			processActions();
		} else if(currentId == 'percent_monet_content') {	// On a modifié le monétaire
			// On traite d'abord les obligations
			processObligations();
			// Puis les actions
			processActions();
		}
		
		valid_form_4();
	});
	
	$('select#exposed_start, select#exposed_end').blur(function(){
		$('select#exposed_start, select#exposed_end').removeClass('error');
		$('select#exposed_start, select#exposed_end').removeClass('valid');
		if($('select#exposed_start').val() > $('select#exposed_end').val()){
			$('select#exposed_start, select#exposed_end').addClass('error');
		}else{
			if($('select#exposed_start').val()!='' && $('select#exposed_end').val()!=''){
				$('select#exposed_start, select#exposed_end').addClass('valid');
			}else{
				$('select#exposed_start, select#exposed_end').addClass('error');
			}
		}
	});
	
	$('#birthday, #birthmonth, #birthyear').change(function(){
		if($('#birthday').val() != '' && $('#birthmonth').val() != '' && $('#birthyear').val() != ''){
			$('#birthday, #birthmonth, #birthyear').removeClass('error');
			if(!verifDate($('#birthyear').val(), $('#birthmonth').val(), $('#birthday').val())){
				$('#birthday, #birthmonth, #birthyear').addClass('error');
			}
		}
	});
	
	$('#birthday, #birthmonth, #birthyear').blur(function(){
		if($('#birthday').val() != '' && $('#birthmonth').val() != '' && $('#birthyear').val() != ''){
			$('#birthday, #birthmonth, #birthyear').removeClass('error');
			if(!verifDate($('#birthyear').val(), $('#birthmonth').val(), $('#birthday').val())){
				$('#birthday, #birthmonth, #birthyear').addClass('error');
			}
		}
	});
	
	$('select[name=birthcountry]').change(function(){
		if($('#green_card').val()==1 || $('select[name=birthcountry]').val()=='États-Unis'){
			$('.us_error').show();
			$('#submit_grp').hide();
		}else{
			$('.us_error').hide();
			var birthday = new Date($('#birthyear').val(), $('#birthmonth').val(), $('#birthday').val(), 0, 0, 0, 0);
			var ageDifMs = Date.now() - birthday.getTime();
			var ageDate = new Date(ageDifMs);
			var age = Math.abs(ageDate.getUTCFullYear() - 1970);
			if(age >= 18){
				$('#submit_grp').show();
			}
		}
	});
	
	$('#green_card_content').click(function(){
		if($('#green_card').val()==1 || $('select[name=birthcountry]').val()=='États-Unis'){
			$('.us_error').show();
			$('#submit_grp').hide();
		}else{
			$('.us_error').hide();
			var birthday = new Date($('#birthyear').val(), $('#birthmonth').val(), $('#birthday').val(), 0, 0, 0, 0);
			var ageDifMs = Date.now() - birthday.getTime();
			var ageDate = new Date(ageDifMs);
			var age = Math.abs(ageDate.getUTCFullYear() - 1970);
			if(age >= 18){
				$('#submit_grp').show();
			}
		}
	});
	
	/*$('#step_form2 select#country').change(function(){
		if($(this).val() == 'États-Unis' && $('#is_fiscal').val() == 1){
			$('#first_address .us_error, .valide.us_error').show();
			$('#submit_grp').hide();
		}else{
			$('#first_address .us_error, .valide.us_error').hide();
			$('#submit_grp').show();
		}
	});
	
	$('#step_form2 select#country_fisc').change(function(){
		if($(this).val() == 'États-Unis' && $('#is_fiscal').val() == 0){
			$('#other_address .us_error, .valide.us_error').show();
			$('#submit_grp').hide();
		}else{
			$('#other_address .us_error, .valide.us_error').hide();
			$('#submit_grp').show();
		}
	});*/
	
	$('#birthday, #birthmonth, #birthyear').click(function(){
		var birthday = new Date($('#birthyear').val(), $('#birthmonth').val(), $('#birthday').val(), 0, 0, 0, 0);
		var ageDifMs = Date.now() - birthday.getTime();
		var ageDate = new Date(ageDifMs);
		var age = Math.abs(ageDate.getUTCFullYear() - 1970);
		if(age < 18){
			$('.minor_error').show();
			$('#submit_grp').hide();
		}else{
			$('.minor_error').hide();
			if($('#green_card').val()==0){
				$('#submit_grp').show();
			}
		}
	});
	
	// Validation du formulaire de modification du mot de passe
	var formPass = $('form#step_password');
	if(formPass.hasClass('step_form')) {
		valid_form_pass();
		$('#step_password input').change(function() {
			valid_form_pass();
		});
	}
	
	// Validation du formulaire de l'étape 1
	var formOne = $('form#step_form1');
	if(formOne.hasClass('step_form')) {
		valid_form_1();
		$('#step_form1 input, #step_form1 select').change(function() {
			valid_form_1();
		});
	}
	
	// Validation du formulaire de l'étape 2
	var formTwo = $('form#step_form2');
	if(formTwo.hasClass('step_form')) {
		valid_form_2();
		$('#step_form2 input, #step_form2 select').change(function() {
			valid_form_2();
		});
		$('#step_form2 input#phone_number, #step_form2 input#phone_number_home, #step_form2 input#phone_number_work, #step_form2 input#phone_number_home2').keyup(function() {
			valid_form_2();
		});
	}
	
	// Validation du formulaire de l'étape 3
	var formThree = $('form#step_form3');
	if(formThree.hasClass('step_form')) {
		valid_form_3();
		$('#step_form3 input, #step_form3 select').change(function() {
			valid_form_3()
		});
	}
	
	// Validation du formulaire de l'étape 4
	var formFour = $('form#step_form4');
	if(formFour.hasClass('step_form')) {
		valid_form_4();
		
		// Doit-on afficher la répartition ?
		var refreshPatrimonyDistribution = function() {
			var show = (
				$('#compte_titre').val() == 1 || 
				$('#pea').val() == 1 || 
				$('#peapme').val() == 1 ||
				$('#assurance_vie').val() == 1 ||
				$('#livret_a').val() == 1 || 
				$('#pee').val() == 1 || 
				$('#compte_courant').val() == 1
			);
			var patrimonyDistribution = $('#patrimony-distribution');
			if(show) {
				patrimonyDistribution.show();
			} else {
				patrimonyDistribution.hide();
			}
		}

		refreshPatrimonyDistribution();

		$('#step_form4 input, #step_form4 select').change(function() {
			valid_form_4();
		});
		
		
		formFour.find('div.form_check, label').click(function() {
			refreshPatrimonyDistribution();
			valid_form_4();
		});
		/*$('#credit_owner_content div.form_check, #credit_owner_content label, #pro_owner_content div.form_check #pro_owner_content, #local_owner_content div.form_check, #second_owner_content div.form_check, #house_owner_content div.form_check').click(function() {
			valid_form_4();
		});*/
		
	}
	
	// Validation du formulaire de l'étape 5
	var formFive = $('form#step_form5');
	if(formFive.hasClass('step_form')) {
		valid_form_5();
		$('#step_form5 input, #step_form5 select').change(function(){
			valid_form_5();
		});
	}
	
	// Validation du formulairaire de l'étape 6
	var formSix = $('form#step_form6');
	if(formSix.hasClass('step_form')) {
		valid_form_6();
		$('#step_form6 input, #step_form6 select').change(function(){
			valid_form_6();
		});
		$('#step_form6 .check_content').click(function(){
			valid_form_6();
		});
	}
	
	// Validation du formulairaire de l'étape 7
	var formSeven = $('form#step_form7');
	if(formSeven.hasClass('step_form')) {
		valid_form_7();
		$('#step_form7 input, #step_form7 select').change(function(){
			valid_form_7();
		});
		$('#step_form7 .check_content').click(function(){
			valid_form_7();
		});
	}
});

function valid_form_pass(){
	var valid = true;
	jQuery('.password_error').hide();
		
	if(jQuery('input#new_password').val() == ''){
		valid = false;
	}
	if(jQuery('input#new_password_confirm').val() == ''){
		valid = false;
	}else{
		jQuery('input#new_password_confirm').removeClass('error');
		jQuery('input#new_password_confirm').parent().removeClass('error');
	}
	if(jQuery('input#new_password_confirm').val() != jQuery('input#new_password').val() && jQuery('input#new_password_confirm').val()!= ''){
		valid = false;
		jQuery('.password_error').show();
		jQuery('input#new_password_confirm').addClass('error');
		jQuery('input#new_password_confirm').parent().addClass('error');
	}
	
	jQuery('#step_password input[type=submit]').attr('disabled', 'disabled');
	/*console.log('valid ?')
	console.log(valid)
	console.log('---')*/
	if(valid == true){
		jQuery('#step_password input[type=submit]').removeAttr('disabled');
	}
}

function valid_form_1(){
	var valid = true;
		
	if(jQuery('input[name=civility]').val() == '') {
		//console.log('Civilité');
		valid = false;
	}
	if(jQuery('input#firstname').val() == '' || jQuery('input#firstname').val().length < 2) {
		valid = false;
		//console.log('Prénom');
	}
	if(jQuery('input#lastname').val() == '' || jQuery('input#lastname').val().length < 2) {
		//console.log('Nom')
		valid = false;
	}
	if(madam){
		//console.log('Mme');
		if(jQuery('input#lastgirlname').val() == '' || jQuery('input#lastgirlname').val().length < 2) valid = false;
	}
	if(jQuery('#birthday').val() != '' && jQuery('#birthmonth').val() != '' && jQuery('#birthyear').val() != '')
	{
		if(!verifDate(jQuery('#birthyear').val(), jQuery('#birthmonth').val(), jQuery('#birthday').val())) {
			valid = false;
			//console.log('Date de naissance')
		}
	} else {
		//console.log('Date de naissance inconnue')
		valid = false;
	}
	if(jQuery('input#birthtown').val() == '' || jQuery('input#birthtown').val().length < 2) {
		valid = false;
		//console.log('Ville')
	}
	if(jQuery('select#birthcountry').val() == '') {
		valid = false;
		//console.log('Pays')
	}
	/*if(jQuery('select#birthdepart').val() == '') {
		valid = false;
	//	console.log('Département');
	}*/
	if(jQuery('select#familial_context').val() == '') {
		valid = false;
		console.log('Status familliale');
	}
	
	if(show_politic){
		/*console.log('show_politic');
		console.log(show_politic);
		console.log('---')*/
		if(jQuery('input#exposed_firstname').val() == '' || jQuery('input#exposed_firstname').val().length < 2) valid = false;
		if(jQuery('input#exposed_lastname').val() == '' || jQuery('input#exposed_lastname').val().length < 2) valid = false;
		if(jQuery('input#exposed_function').val() == '' || jQuery('input#exposed_function').val().length < 2) valid = false;
		if(jQuery('select#exposed_country').val() == '') valid = false;
		if(jQuery('select#exposed_start').val() == '') valid = false;
		if(jQuery('select#exposed_end').val() == '') valid = false;
		if(jQuery('select#exposed_start').val() > jQuery('select#exposed_end').val()) valid = false;
	}
	
	jQuery('#step_form1 input[type=submit]').attr('disabled', 'disabled');
	/*console.log('valid ?')
	console.log(valid)
	console.log('---')*/
	if(valid == true){
		jQuery('#step_form1 input[type=submit]').removeAttr('disabled');
	}
}

function valid_form_2() {
	
	var valid = true;
	
	if(jQuery('input#address_1').val() == '' || jQuery('input#address_1').val().length < 2) valid = false;
	if(jQuery('input#address_2').val().length > 0 && jQuery('input#address_2').val().length < 2) valid = false;
	if(jQuery('input#town').val() == '' || jQuery('input#town').val().length < 2) valid = false;
	if(jQuery('input#zipcode').val() == '' || jQuery('input#zipcode').val().length < 2) valid = false;
	if(jQuery('select#country').val() == '') valid = false;
	

	if(jQuery('input#phone_number').val() == '' || !isValidNumber(jQuery('input#phone_number').val(), country_code)) {
		valid = false;
		//console.log('invalid phone mobile');
	}
	if(jQuery('input#phone_number_home').val() != '' && !isValidNumber(jQuery('input#phone_number_home').val(), country_code)) valid = false;
	if(jQuery('input#phone_number_work').val() != '' && !isValidNumber(jQuery('input#phone_number_work').val(), country_code)) valid = false;
	if(jQuery('input#phone_number_home2').val() != '' && !isValidNumber(jQuery('input#phone_number_home2').val(), country_code)) valid = false;
	//if(jQuery('input#email').val() == '' || verifEmail(jQuery('input#email').val())) valid = false;
	
	
	var fiscaleAddressRequired = (jQuery('input#is_fiscal').val() == 0);
	
	if(fiscaleAddressRequired){
		if(jQuery('input#address_fisc_1').val() == '' || jQuery('input#address_fisc_1').val().length < 2) valid = false;
		if(jQuery('input#address_fisc_2').val().length > 0 && jQuery('input#address_fisc_2').val().length < 2) valid = false;
		if(jQuery('input#town_fisc').val() == '' || jQuery('input#town_fisc').val().length < 2) valid = false;
		if(jQuery('input#zipcode_fisc').val() == '' || jQuery('input#zipcode_fisc').val().length < 2) valid = false;
		if(jQuery('select#country_fisc').val() == '') valid = false;
	}
	
	jQuery('#step_form2 input[type=submit]').attr('disabled', 'disabled');
	if(valid == true){
		jQuery('#step_form2 input[type=submit]').removeAttr('disabled');
	}
}

function valid_form_3(){
	var valid = true;
	
	if(jQuery('input#nature_pro').val() == 0 && jQuery('input#nature_foncier').val() == 0 && jQuery('input#nature_retraite').val() == 0 && jQuery('input#nature_others').val() == 0) {
		valid = false;
	}

	if(show_pro){
		var psc = jQuery('select#Profession_INSEE__c').val();
		if(psc == '') {
			valid = false;
		}
		
		// Gestion de l'ancien secteur d'activité
		if(psc == 'Retraités' || psc == 'Anciens employés et ouvriers' || psc == 'Anciens artisans, commerçants, chefs d\'entreprise' || psc == 'Anciens cadres et professions intermédiaires') {
			jQuery('div#job_block').show();
			// Dans ce cas l'ancien secteur d'activité est obligatoire
			var oldJob = jQuery('input#old_job').val();
			if(oldJob == '' || oldJob.length < 2) {
				valid = false;
			}
			
		} else {
			jQuery('div#job_block').hide();
		}
		
		if(jQuery('input#job').val() == '' || jQuery('input#job').val().length < 2) {
			valid = false;
		}
		
		// Gestion de la société
		var categoiesWithSociety = [
			'Chefs d\'entreprise de 10 salariés ou plus',
			'Cadres et professions intellectuelles supérieures',
			'Professions Intermédiaires',
			'Employés',
			'Ouvriers'
		];
		// La société est obligatoire
		if(jQuery.inArray(psc, categoiesWithSociety) != -1) {
			jQuery('div#society_block').show();
			if(jQuery('input#society').val() == '' || jQuery('input#society').val().length < 2) {
				valid = false;
			}
		} else {
			jQuery('div#society_block').hide();
		}
	}
	
	if(show_other){
		if(jQuery('input#amount_nature').val() == '' || jQuery('input#amount_nature').val().length < 2) {
			valid = false;
		}
	}
	
	jQuery('#step_form3 input[type=submit]').attr('disabled', 'disabled');
	//console.log(valid);
	if(valid == true){
		jQuery('#step_form3 input[type=submit]').removeAttr('disabled');
	}
}

function valid_form_4(){
	var valid = true;
	
	
	// Il faut au moins un type de compte
	var accountSelected = (
		jQuery('#compte_titre').val() == 1 || 
		jQuery('#pea').val() == 1 || 
		jQuery('#peapme').val() == 1 ||
		jQuery('#assurance_vie').val() == 1 ||
		jQuery('#livret_a').val() == 1 || 
		jQuery('#pee').val() == 1 || 
		jQuery('#compte_courant').val() == 1
	);
	if(!accountSelected) {
		valid = false;
	}
		
	if(jQuery('#compte_titre').val() == 1){
		if(jQuery('select#compte_titre_type').val() == '') {
			valid = false;
		}
		if(jQuery('input#compte_titre_amount').val() == '' || !verifNumber(jQuery('input#compte_titre_amount').val())) {
			valid = false;
		}
		if(jQuery('input#compte_titre_location').val() == '' || jQuery('input#compte_titre_location').val().length < 2) {
			valid = false;
		}
	}
	if(jQuery('#pea').val() == 1){
		if(jQuery('select#pea_type').val() == '') {
			valid = false;
		}
		if(jQuery('input#pea_amount').val() == '' || !verifNumber(jQuery('input#pea_amount').val())) {
			valid = false;
		}
		if(jQuery('input#pea_location').val() == '' || jQuery('input#pea_location').val().length < 2) {
			valid = false;
		}
	}
	if(jQuery('#peapme').val() == 1){
		if(jQuery('select#peapme_type').val() == '') {
			valid = false;
		}
		if(jQuery('input#peapme_amount').val() == '' || !verifNumber(jQuery('input#peapme_amount').val())) {
			valid = false;
		}
		if(jQuery('input#peapme_location').val() == '' || jQuery('input#peapme_location').val().length < 2) {
			valid = false;
		}
	}
	if(jQuery('#livret_a').val() == 1){
		if(jQuery('input#livret_a_amount').val() == '' || !verifNumber(jQuery('input#livret_a_amount').val())) {
			valid = false;
		}
		if(jQuery('input#livret_a_location').val() == '' || jQuery('input#livret_a_location').val().length < 2) {
			valid = false;
		}
	}
	if(jQuery('#pee').val() == 1){
		if(jQuery('input#pee_amount').val() == '' || !verifNumber(jQuery('input#pee_amount').val())) {
			valid = false;
		}
		if(jQuery('input#pee_location').val() == '' || jQuery('input#pee_location').val().length < 2) {
			valid = false;
		}
	}
	if(jQuery('#compte_courant').val() == 1){
		if(jQuery('input#compte_courant_amount').val() == '' || !verifNumber(jQuery('input#compte_courant_amount').val())) {
			valid = false;
		}
		if(jQuery('input#compte_courant_location').val() == '' || jQuery('input#compte_courant_location').val().length < 2) {
			valid = false;
		}
	}
	if(jQuery('#assurance_vie').val() == 1){
		if(jQuery('select#assurance_vie_type').val() == '') {
			valid = false;
		}
		if(jQuery('input#assurance_vie_amount').val() == '' || !verifNumber(jQuery('input#assurance_vie_amount').val())) {
			valid = false;
		}
		if(jQuery('input#assurance_vie_location').val() == '' || jQuery('input#assurance_vie_location').val().length < 2) {
			valid = false;
		}
	}
	
	if(jQuery('#house_owner').val() == 1){
		if(jQuery('input#house_owner_amount').val() == '' || !verifNumber(jQuery('input#house_owner_amount').val())) {
			valid = false;
		}
	}
	if(jQuery('#second_owner').val() == 1){
		if(jQuery('input#second_owner_amount').val() == '' || !verifNumber(jQuery('input#second_owner_amount').val())) {
			valid = false;
		}
	}
	if(jQuery('#local_owner').val() == 1){
		if(jQuery('input#local_owner_amount').val() == '' || !verifNumber(jQuery('input#local_owner_amount').val())) {
			valid = false;
		}
	}
	if(jQuery('#pro_owner').val() == 1){
		if(jQuery('input#pro_owner_amount').val() == '' || !verifNumber(jQuery('input#pro_owner_amount').val())) {
			valid = false;
		}
	}
	if(jQuery('#credit_owner').val() == 1){
		
		// Validation de la date d'échéance du prêt
		var loanMaturity = jQuery('#Endettement_Echeance__c').val();
		if(loanMaturity == '' && !isYear(loanMaturity)) {
			valid = false;
		}
		
		if(jQuery('input#credit_owner_amount').val() == '' || !verifNumber(jQuery('input#credit_owner_amount').val())) {
			valid = false;
		}
	}
	
	var total = parseFloat(jQuery('#percent_action').val()) + parseFloat(jQuery('#percent_oblig').val()) + parseFloat(jQuery('#percent_monet').val());
	if(total != 100){
		valid = false;
	}
	
	jQuery('#step_form4 input[type=submit]').attr('disabled', 'disabled');
	//console.log(valid);
	//console.log('---')
	if(valid){
		jQuery('#step_form4 input[type=submit]').removeAttr('disabled');
	}
}

function valid_form_5(){
	
	var valid = true;
		
	if(jQuery('select#relationship_origin').val() == '') valid = false;
	if(jQuery('select#relationship_origin').val() == 'collaborateur'){
		if(jQuery('input#collaborateur').val() != '' && verifEmail(jQuery('input#collaborateur').val())) valid = false;
	}

	//console.log(jQuery('input[name=demarche]').val())

	jQuery('#step_form5 input[type=submit]').attr('disabled', 'disabled');
	if(valid){
		jQuery('#step_form5 input[type=submit]').removeAttr('disabled');
	}
	
}

function valid_form_6(){
	
	var $ = jQuery;
	
	var valid = true;
	
		
	var empties = {
		type: false,
		amount: false,
		bank: false
	};
	
	var fields = {
		type: $('select#account_type'),
		amount: $('input#account_amount'),
		bank: $('input#account_from'),
		precision1: $('input#Heritage_Desc__c'),
		precision2: $('input#Donation_Desc__c'),
		precision3: $('input#Vente_immobilier_Desc__c'),
		precision4: $('input#Autres_Desc__c'),
		objective: $('select#account_objective'),
		objectivePrecision: $('input#account_objective_precision'),
		//subscribe: $('input[name=account_subscribe]'),		
		method: $('select[name=account_method]'),		
		bank2: $('input[name=TR_Etablissement__c]'),
		addr: $('input[name=TR_Adresse_L1__c]'),
		zipcode: $('input[name=TR_CP__c]'),
		town: $('input[name=TR_Ville__c]'),
		country: $('select[name=TR_Pays__c]'),
		phone: $('input[name=TR_Tel_Agence__c]'),
		name: $('input[name=TR_Nom__c]'),
		lastname: $('input[name=TR_Prenom__c]'),
		iban: $('input[name=TR_IBAN__c]'),
		bic: $('input[name=TR_BIC__c]')
	};
	
	/**
	 * Fonction de validation d'un champs dont on donne la clé correspondante dans le tableau field
	 */
	var validation = function(type) {
	
		var field = fields[type];
		
		if(!field) {
			return false;
		}
		
		var value = field.val();
		var valid = false;
	
		switch(type) {
			case 'type':
				valid = (value != '');
			break;
			case 'method':
				valid = (value != '');
			break;
			case 'label':
				valid = (value != '' && value.length >= 2);
			break;
			case 'amount':
				valid = (value != '' && verifNumber(value));
				if(valid) {
					value = value.replace(/ /g,"");
					value = parseFloat(value);
					var accountType = fields.type.val();
					// Vérification de la valeur pour un compte PEA et PEA-PME
					if((accountType == 'Compte PEA' && value > 152000) || (accountType == 'Compte PEA-PME' && value > 75000)) {
						valid = false;
						$('input#account_amount').addClass('error');
						$('input#account_amount').removeClass('valid');
					}else{
						$('input#account_amount').removeClass('error');
						$('input#account_amount').addClass('valid');
					}
				}
			break;
			case 'bank':
				valid = (value != '' && value.length >= 2);
			break;
			case 'origin':
				valid = (value != '');
			break;
			case 'precision1':
				if($('#Heritage__c').val() == 0){
					valid = true;
				}else{
					if(value != '' && value.length >= 2){
						valid = true;
					}
				}
			break;
			case 'precision2':
				if($('#Donation__c').val() == 0){
					valid = true;
				}else{
					if(value != '' && value.length >= 2){
						valid = true;
					}
				}
			break;
			case 'precision3':
				if($('#Vente_immobilier__c').val() == 0){
					valid = true;
				}else{
					if(value != '' && value.length >= 2){
						valid = true;
					}
				}
			break;
			case 'precision4':
				if($('#Autres__c').val() == 0){
					valid = true;
				}else{
					if(value != '' && value.length >= 2){
						valid = true;
					}
				}
			break;
			case 'objective':
				valid = (value != '');
			break;
			case 'objectivePrecision':
				valid = (fields.objective.val() != 'other' || (value != '' && value.length >= 2));
			break;
			case 'subscribe':
				valid = subscribe;
			break;
			case 'bank2':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length >= 2);
				}
			break;
			case 'addr':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length >= 2);
				}
			break;
			case 'zipcode':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length >= 2);
				}
			break;
			case 'town':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length >= 2);
				}
			break;
			case 'country':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '');
				}
			break;
			case 'phone':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && verifPhone(value));
				}
			break;
			case 'name':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length >= 2);
				}
			break;
			case 'lastname':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length >= 2);
				}
			break;
			case 'iban':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && verifIBAN(value));
				}
			break;
			case 'bic':
				valid = true;
				if($('#account_method').val()=='transfert de titres'){
					valid = (value != '' && value.length <= 11);
				}
			break;
		}

		if(!valid && empties[type] != undefined) {
			empties[type] = true;
		}

		return valid;
	};
	
	// Validation des champs et déterminé
	$.each(fields, function(fieldType) {
		var validField = validation(fieldType);
		
		//console.log(fieldType+' '+validField)
		if(!validField) {
			valid = false;
		}
	});
	
	// Validation de la partie horizon / mandat
	if($('select#account_type').val() == 'Compte Titres'){
		if(horizon == '') valid = false;
		if(horizon == 'Court - moyen terme (> 2 ans)' && !subscribe2) valid = false;
		if(horizon == 'Moyen - long terme (> 5 ans)' && !risk) valid = false;
	}else{
		if(!subscribe) valid = false;
	}
	
	//console.log('---')
		
	// On détermine si le formulaire a été remplit
	var empty = true;
	$.each(empties, function(index, value) {
		if(!value) {
			//console.log(index);
			empty = false;
		}
	});
	
	var selectSubmit = '#step_form6 input[type=submit]';
	// Le formulaire est vide, on n'est pas obligé de créer un compte
	if(empty) {
		selectSubmit += '.create-account';
	} 
	
	var inputSubmit = $(selectSubmit);

	inputSubmit.attr('disabled', 'disabled');
	if(valid == true) {
		inputSubmit.removeAttr('disabled');
	}
	
	// On compte le nombre de compte
	var countAccounts = $('div.account_line').length;
	/*console.log(empty)
	console.log(countAccounts)*/
	if(empty && countAccounts > 0) {
		$('#step_form6 input[type=submit].finish').removeAttr('disabled');
	}
	
}

function valid_form_7(){
	
	var $ = jQuery;
	var valid = true;
	jQuery.each(files, function(index, file){
                var input = $(file);
		var fileFieldset = input.parents('fieldset.document_fieldset');
                if(fileFieldset) {
                    fileFieldset.removeClass('error');
                    if(input.val() == '') {
                        fileFieldset.addClass('error');
                        valid = false;
                    }
                }
	});
	
	switch(identity){
		case 'identity_card':
			var file = jQuery('input[name=file-identity_card_r]');
			var fileFieldset = file.parents('fieldset.document_fieldset');
			fileFieldset.removeClass('error');
			if(file.val() == '') {
				fileFieldset.addClass('error');
				valid = false;
			}
			var file = jQuery('input[name=file-identity_card_v]');
			var fileFieldset = file.parents('fieldset.document_fieldset');
			fileFieldset.removeClass('error');
			if(file.val() == '') {
				fileFieldset.addClass('error');
				valid = false;
			}
			break;
		case 'identity_pass':
			var file = jQuery('input[name=file-identity_pass]');
			var fileFieldset = file.parents('fieldset.document_fieldset');
			fileFieldset.removeClass('error');
			if(file.val() == '') {
				fileFieldset.addClass('error');
				valid = false;
			}
			break;
		case 'identity_visa':
			var file = jQuery('input[name=file-identity_visa_r]');
			var fileFieldset = file.parents('fieldset.document_fieldset');
			fileFieldset.removeClass('error');
			if(file.val() == '') {
				fileFieldset.addClass('error');
				valid = false;
			}
			var file = jQuery('input[name=file-identity_visa_v]');
			var fileFieldset = file.parents('fieldset.document_fieldset');
			fileFieldset.removeClass('error');
			if(file.val() == '') {
				fileFieldset.addClass('error');
				valid = false;
			}
			break;
	}
	
	
	var selectSubmit = '#step_form7 input[type=submit]';
	var inputSubmit = $(selectSubmit);

	inputSubmit.attr('disabled', 'disabled');
	if(valid == true) {
		inputSubmit.removeAttr('disabled');
	}
}

