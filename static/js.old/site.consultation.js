function isFlash(){
	if( navigator.mimeTypes.length > 0){
		return navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin != null;
	}else if( window.ActiveXObject ){
		try{
			new ActiveXObject( "ShockwaveFlash.ShockwaveFlash" );
			return true;
		}catch( oError ){
			return false;
		}
	}else{
		return false;
	}
}

function isTablet() {
	/*try {
		return (window.matchMedia('(max-width: 1200px)').matches);
	} catch(error) {
		return false;
	}*/
	return (support == 'tablet' || support == 'phone');
}

function isMobile() {
	return (support == 'phone');
}

function scrollTo(id){
	jQuery('html, body').animate({
		scrollTop: jQuery("#"+id).offset().top
	}, 500);
}

function number_format(number, decimals, dec_point, thousands_sep) {
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function(n, prec) {
		var k = Math.pow(10, prec);
		return '' + (Math.round(n * k) / k)
		.toFixed(prec);
	};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}

function reset_data($){
	var ids = accounts.split(',');
	var position = 0;
	var perf = 0;
	var perfp = 0;
	var ar = 0;
	var apports = 0;
	var retraits = 0;
	var frais = 0;
	var index;
	
	for(index = 0; index < ids.length; index++){
		if(ids[index] != '0' && ids[index] != 0){
			position+= parseFloat($('#position_'+ids[index]).val());
			perf+= parseFloat($('#perfeur_'+filter+'_'+ids[index]).val());
			perfp+= parseFloat($('#perf_'+filter+'_'+ids[index]).val());
			apports+= parseFloat($('#apport_'+filter+'_'+ids[index]).val());
			retraits+= parseFloat($('#retrait_'+filter+'_'+ids[index]).val());
			frais+= parseFloat($('#frais_'+filter+'_'+ids[index]).val());
			ar+= parseFloat($('#ar_'+filter+'_'+ids[index]).val());
		}		
	}
	
	$('#synthesis_position > .full_number').html(number_format(position, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_apports > .full_number').html(number_format(apports, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_retraits > .full_number').html(number_format(retraits, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_frais > .full_number').html(number_format(frais, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_perf > .full_number').html(number_format(perf, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_ar > .full_number').html(number_format(ar, 0, ',', ' ')+' <span class="decimal">€</span>');
	
	$('.account_perf_pos').attr('data-original-title', 'SOMME DE VOS POSITIONS<br>La somme de votre/vos compte(s) séléctionné(s) au '+date+' est de '+number_format(position, 0, ',', ' ')+' <span class="decimal">€</span><br><br>PERFOMANCE DE VOS ACTIFS<br>Au cours de la période, la performance de votre/vos compte(s) séléctionné(s) est de '+number_format(perf, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_apports').attr('data-original-title', 'APPORTS EFFECTUÉS<br>Au cours de la période, la somme des apports effectués sur votre/vos compte(s) sélectionné(s) est de '+number_format(apports, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_retraits').attr('data-original-title', 'RETRAITS EFFECTUÉS<br>Au cours de la période, la somme des retraits effectués sur votre/vos compte(s) sélectionné(s) est de '+number_format(retraits, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_frais').attr('data-original-title', 'FRAIS PRÉLEVÉS<br>Au cours de la période, la somme des frais prélevés sur votre/vos compte(s) sélectionné(s) est de '+number_format(frais, 0, ',', ' ')+' <span class="decimal">€</span>');
	$('#synthesis_ar').attr('data-original-title', 'APPORTS / RETRAITS<br>Au cours de la période, la somme des apports/retraits effectués sur votre/vos compte(s) sélectionné(s) est de '+number_format(ar, 0, ',', ' ')+' <span class="decimal">€</span>');
	
	$('#synthesis_perf').removeClass('negative');
	if(perf <0){
		$('#synthesis_perf').addClass('negative');
	}
	
	/*perfp = number_format(perfp, 2, ',', ' ');
	perfp = perfp.split(',');
	perfp = perfp[0] + ',<span class="decimal">' + perfp[1] + '</span> <span class="decimal">%</span>';
	$('#synthesis_perf .percent > .full_number').html(perfp);*/
}

var activity = false;
var sidebarClient = false;
var sidebarClientHideClick = false;

jQuery(document).ready(function($){	
	$('#toggle_sidebar').click(function(){
		if(!sidebarClient && !sidebarClientHideClick){
			$( "#sidebar_client").slideDown();
			sidebarClient = true;
		}else{
			$( "#sidebar_client").slideUp();
			sidebarClient = false;
			sidebarClientHideClick = false;
		}
	});	
	
	$(document).mouseup(function (e){
		var container = $("#activity_block_container");
		if(container.has(e.target).length === 0 && activity){
			$("#activity_block").animate({width:"0px"}, 500, function(){activity = false;});
			$("#activity_block_container").width("0px");
		}
		
		var container = $("#sidebar_client");
		var button = $("#toggle_sidebar");
		if(container.has(e.target).length === 0 && button.has(e.target).length === 0 && jQuery('body').width() <= 768 && sidebarClient){
			container.hide();
			sidebarClient = false;
			sidebarClientHideClick = true;
			setTimeout(function(){sidebarClientHideClick = false;}, 500);
		}
	});
	
	$('a.mouv_load_more').click(function(){
		mouv_block_3 = mouv_block_3 + 1;
		$('table#mouvements tr.mouv_block_'+mouv_block_3+', table#mouvements_mobile tr.mouv_block_'+mouv_block_3).show();
		if(mouv_block_3 == max_mouv_block_3){
			$('.mouv_load_more').hide();
		}
	});
	
	$('.tooltip-input').tooltip({'placement':'top', 'trigger':'focus', 'html':true});
	$('.tooltip_top').tooltip({'placement':'top', 'html':true});
	$('.tooltip_bottom, .account_perf_pos, .account_synthesis > .account_block_number').tooltip({'placement':'bottom', 'html':true});	
	
	$('a#message_link').click(function(){
		$("#activity_block").animate({width:"300px"}, 500, function(){activity = true;});
		$("#activity_block_container").width("400px");
		if($(this).hasClass('unread')){
			$(this).removeClass('unread');
			$(this).addClass('read');
			$(this).html('');
			
			$.ajax({
				url: base_url+"activities/read",
				type: 'POST',
				success: function(msg){}
			}); 
		}
	});
	
	$('a#close_activity_block').click(function(){
		$("#activity_block").animate({width:"0px"}, 500, function(){activity = false;});
		$("#activity_block_container").width("0px");
	});
	
	$('a#close_sidebar_block').click(function(){
		$( "#sidebar_client" ).slideUp();
	});
		
	if($('#breadcrump-nav-selection').length){
		$('#breadcrump-nav li').click(function(){
			if($(this).attr('id') != 'mother-section'){
				scrollTo($(this).attr('target'));
			}
		});
		
		var selected_menu = $('#breadcrump-nav li[target=breadcrump_target_1]');
		var breadcrump_arrow = $('#breadcrump-nav-selection');
		var left = selected_menu.position().left + (selected_menu.width() / 2);
		breadcrump_arrow.css('left', left);
		
		$(window).scroll(function(){
			posScroll = $(document).scrollTop();
			
			var previous = $('#breadcrump_target_1');
			$('.breadcrump_target').each(function(){
				if(posScroll >= ($(this).position().top - 30)){
					previous = $(this);
				}else{
					return false;
				}
			});
			
			var current_menu = $('#breadcrump-nav li[target='+previous.attr('id')+']');
			if(current_menu.length){
				if(selected_menu.position().left != current_menu.position().left){
					var left = current_menu.position().left + (current_menu.width() / 2);
					breadcrump_arrow.animate({left:left}, 100);
					selected_menu = current_menu;
				}
			}
		});
	}
	
	$('input[name=graph_period]').change(function(){
		$('.filter_0, .filter_1, .filter_2, .filter_3').hide();
		$('.filter_'+$(this).val()).show();
		$('.account_block_number.filter_'+$(this).val()).attr('style', 'display:inline-block;');
		filter = $(this).val();
		load_data();
		/*if($('#movements_wrapper').length){
			$.ajax({
				url: full_url+"account/movements/"+account_number,
				type: 'POST',
				data: 'filter='+filter,
				success: function(msg){
					$('#movements_wrapper').html(msg);
				}
			});
		}*/
		if($('input[name=graph_period2]').length){
			switch($(this).val()){
				case '0':
					$('#graph_period22, #graph_period23, #graph_period24').removeAttr('checked', 'checked');
					$('#graph_period21').attr('checked', 'checked');
					break;
				case '1':
					$('#graph_period21, #graph_period23, #graph_period24').removeAttr('checked', 'checked');
					$('#graph_period22').attr('checked', 'checked');
					break;
				case '2':
					$('#graph_period22, #graph_period21, #graph_period24').removeAttr('checked', 'checked');
					$('#graph_period23').attr('checked', 'checked');
					break;
				case '3':
					$('#graph_period22, #graph_period23, #graph_period21').removeAttr('checked', 'checked');
					$('#graph_period24').attr('checked', 'checked');
					break;
			}
		}
		
		if($('.desktop_account').length){
			reset_data($);
		}
	});
	
	$('input[name=graph_period2]').change(function(){
		$('.filter_0, .filter_1, .filter_2, .filter_3').hide();
		$('.filter_'+$(this).val()).show();
		$('.account_block_number.filter_'+$(this).val()).attr('style', 'display:inline-block;');
		filter = $(this).val();
		load_data();
		
		switch($(this).val()){
			case '0':
				$('#graph_period2, #graph_period3, #graph_period4').removeAttr('checked', 'checked');
				$('#graph_period1').attr('checked', 'checked');
				break;
			case '1':
				$('#graph_period1, #graph_period3, #graph_period4').removeAttr('checked', 'checked');
				$('#graph_period2').attr('checked', 'checked');
				break;
			case '2':
				$('#graph_period2, #graph_period1, #graph_period4').removeAttr('checked', 'checked');
				$('#graph_period3').attr('checked', 'checked');
				break;
			case '3':
				$('#graph_period2, #graph_period3, #graph_period1').removeAttr('checked', 'checked');
				$('#graph_period4').attr('checked', 'checked');
				break;
		}
	});
	
	if($('#funds_iframe').length){
		if($('body').width() > 768){
			$('#funds_iframe').attr('style', 'padding-left:220px');
		}else{
			$('#funds_iframe').attr('style', 'padding-left:0');
		}
	}
	
	if($('#account_top_right').length){
		if($('body').width() > 1200){
			$('#account_top_right .segmented-control').addClass('medium_size');
			$('#account_top_right .segmented-control').removeClass('full_size');
		}else{
			$('#account_top_right .segmented-control').removeClass('medium_size');
			$('#account_top_right .segmented-control').addClass('full_size');
		}
	}
	
	$(window).on('resize', function(){
		if($('#funds_iframe').length){
			if($('body').width() > 768){
				$('#funds_iframe').attr('style', 'padding-left:220px');
			}else{
				$('#funds_iframe').attr('style', 'padding-left:0');
			}
		}
		
		if($('#account_top_right').length){
			if($('body').width() > 1200){
				$('#account_top_right .segmented-control').addClass('medium_size');
				$('#account_top_right .segmented-control').removeClass('full_size');
			}else{
				$('#account_top_right .segmented-control').removeClass('medium_size');
				$('#account_top_right .segmented-control').addClass('full_size');
			}
		}
	});
});

function valid_form_pass(){
	var valid = true;
	jQuery('.password_error').hide();
		
	if(jQuery('input#new_password').val() == ''){
		valid = false;
	}
	if(jQuery('input#new_password_confirm').val() == ''){
		valid = false;
	}else{
		jQuery('input#new_password_confirm').removeClass('error');
		jQuery('input#new_password_confirm').parent().removeClass('error');
	}
	if(jQuery('input#new_password_confirm').val() != jQuery('input#new_password').val() && jQuery('input#new_password_confirm').val()!= ''){
		valid = false;
		jQuery('.password_error').show();
		jQuery('input#new_password_confirm').addClass('error');
		jQuery('input#new_password_confirm').parent().addClass('error');
	}
	
	jQuery('#step_password input[type=submit]').attr('disabled', 'disabled');
	/*console.log('valid ?')
	console.log(valid)
	console.log('---')*/
	if(valid == true){
		jQuery('#step_password input[type=submit]').removeAttr('disabled');
	}
}