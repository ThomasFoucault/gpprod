if(support == 'phone'){
	// On enlève les élements du menu qui ne servent pas
	$('ul.navbar-nav li:nth-child(1) > a .caret, ul.navbar-nav li:nth-child(1) ul, ul.navbar-nav li:nth-child(2) > a .caret, ul.navbar-nav li:nth-child(2) ul, ul.navbar-nav li:nth-child(3) > a .caret, ul.navbar-nav li:nth-child(3) ul').remove();
	$('ul.navbar-nav li:nth-child(1) > a, ul.navbar-nav li:nth-child(2) > a, ul.navbar-nav li:nth-child(3) > a').attr('data-toggle', '');
	$('ul.navbar-nav li:nth-child(1) > a, ul.navbar-nav li:nth-child(2) > a, ul.navbar-nav li:nth-child(3) > a').attr('data-hover', '');
	$('ul.navbar-nav:first-child li:nth-child(1) > a, ul.navbar-nav:first-child li:nth-child(2) > a, ul.navbar-nav:first-child li:nth-child(3) > a').attr('class', '');
	
	$('div.navbar-collapse #fondation_link, divnavbar-collapse ul.navbar-right').remove();
	
	if(lang=='fr'){
		$('ul.navbar-nav li:nth-child(4)').append($('ul.navbar-nav li:nth-child(4) li a'));
		$('ul.navbar-nav li:nth-child(4) ul, ul.navbar-nav li:nth-child(4) a:first-child').remove();
	}
}