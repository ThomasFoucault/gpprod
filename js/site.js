function scrollTo(id){
	if(id){
		jQuery('html, body').animate({
			scrollTop: jQuery("#"+id).offset().top
		}, 500);
	}
}

function extractUrlParams(){	
	var t = location.search.substring(1).split('&');
	var f = [];
	for (var i=0; i<t.length; i++){
		var x = t[ i ].split('=');
		f[x[0]]=x[1];
	}
	return f;
}

function showActivitiesBlock($){
	$("#activity_block").animate({width:"300px"}, 500, function(){activity = true;$("#activity_block").attr("style", "width:300px;overflow:auto;");});
	$("#activity_block_container").attr("style", "width:400px;overflow:auto;");
	
	if($('a#message_link').hasClass('unread')){
		$('a#message_link').removeClass('unread');
		$('a#message_link').addClass('read');
		$('a#message_link').html('');
	}
	
	$('a#close_activity_block').click(function(){
		$("#activity_block").animate({width:"0px"}, 500, function(){activity = false;$("#activity_block").attr("style", "width:0px;overflow:hidden;");});
		$("#activity_block_container").attr("style", "width:0px;overflow:hidden;");
	});
}

function breadcrump($){
	setTimeout(function(){
		$('#breadcrump-nav li').click(function(){
			if($(this).attr('id') != 'mother-section'){
				scrollTo($(this).attr('target'));
			}
		});
		
		var selected_menu = $('#breadcrump-nav li[target=breadcrump_target_1]');
		if(selected_menu.length){
			var breadcrump_arrow = $('#breadcrump-nav-selection');
			var left = selected_menu.position().left + (selected_menu.width() / 2);
			breadcrump_arrow.css('left', left);
		}
		
		$(window).scroll(function(){
			posScroll = $(document).scrollTop();
			
			var previous = $('#breadcrump_target_1');
			if(previous.length){
				$('.breadcrump_target').each(function(){
					if(posScroll >= ($(this).position().top - 30)){
						previous = $(this);
					}else{
						return false;
					}
				});
			}
			
			var current_menu = $('#breadcrump-nav li[target='+previous.attr('id')+']');
			if(current_menu.length){
				if(selected_menu.position().left != current_menu.position().left){
					var left = current_menu.position().left + (current_menu.width() / 2);
					breadcrump_arrow.animate({left:left}, 100);
					selected_menu = current_menu;
				}
			}
		});
	}, 500);
}

function refreshAddPhone($){
	var countPhones = 0
	$('fieldset.fieldset-phone div.form-group').each(function() {
		var phone = $(this);
		if(phone.attr('id') != 'add_phone' && phone.css('display') == 'block') {
			countPhones++;
		}
	});
	if(countPhones == 4) {
		$('#add_phone').hide();
	}
}

function verifEmail(email) {
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	return (email == '' || !reg.test(email));
}

function verifText(text){
	var reg = /^\D+$/;
	return (text == '' || !reg.test(text));
}

function profileInformations($, minorChildValue, majorChildValue, club){		
	// Sliders
	if(!club){
		var slider_minor_child = jQuery('#minor_child_content').slider({
			min: 0,
			max: 10,
			range: "min",
			value: minorChildValue,
			slide: function( event, ui ) {
				jQuery('#minor_child').val(ui.value);
				jQuery('#minor_child_number').html(ui.value);
				profileCheck($);
			}
		});
		
		var slider_major_child = jQuery('#major_child_content').slider({
			min: 0,
			max: 10,
			range: "min",
			value: majorChildValue,
			slide: function( event, ui ) {
				jQuery('#major_child').val(ui.value);
				jQuery('#major_child_number').html(ui.value);
				profileCheck($);
			}
		});
	}
	
	// Téléphones	
	refreshAddPhone($);
	
	$('#add_phone button').click(function(){
		$('#add_phone ul').show();
	});
	
	$('#add_phone a').click(function(){
		$('.form-group#phone_'+$(this).attr('rel')).show();
		refreshAddPhone($);
		profileCheck($);
		$(this).hide();
	});
	
	// Contrôles
	$('input').keyup(function(){
		$(this).removeClass('error');
		$(this).removeClass('valid');
		$(this).parent().removeClass('error');
		$(this).parent().removeClass('valid');
		$(this).parent().parent().removeClass('error');
		$(this).parent().parent().removeClass('valid');
		
		var correct = true;
		
		if($(this).hasClass('required') && $(this).val()==''){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			if($(this).hasClass('form-select')){
				$(this).parent().parent().addClass('error');
			}
			correct = false;
		}
		
		if($(this).hasClass('control-text') && $(this).val().length < 2 && $(this).val().length > 0){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-email') && $(this).val()!='' && verifEmail($(this).val())){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-phone') && $(this).val()!='' && !isValidNumber($(this).val(), 'FR')){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-portable') && $(this).val()!='' && !isValidNumber($(this).val(), 'FR')){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if(correct){
			$(this).addClass('valid');
			$(this).parent().addClass('valid');
			if($(this).hasClass('control-phone') || $(this).hasClass('control-portable') || $(this).hasClass('control-number')){
				$(this).parent().parent().addClass('valid');
			}
			if($(this).hasClass('form-select')){
				$(this).parent().parent().addClass('valid');
			}
			
			profileCheck($);
		}
	});
	
	// Submit
	$('#step_infos').submit(function(event){
		event.preventDefault();
	});
}

function profileClubInformations($){		
	// Contrôles
	$('input').keyup(function(){
		$(this).removeClass('error');
		$(this).removeClass('valid');
		$(this).parent().removeClass('error');
		$(this).parent().removeClass('valid');
		$(this).parent().parent().removeClass('error');
		$(this).parent().parent().removeClass('valid');
		
		var correct = true;
		
		if($(this).hasClass('required') && $(this).val()==''){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			if($(this).hasClass('form-select')){
				$(this).parent().parent().addClass('error');
			}
			correct = false;
		}
		
		if($(this).hasClass('control-text') && $(this).val().length < 2 && $(this).val().length > 0){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-email') && $(this).val()!='' && verifEmail($(this).val())){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-phone') && $(this).val()!='' && !isValidNumber($(this).val(), 'FR')){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if($(this).hasClass('control-portable') && $(this).val()!='' && !isValidNumber($(this).val(), 'FR')){
			$(this).addClass('error');
			$(this).parent().addClass('error');
			$(this).parent().parent().addClass('error');
			correct = false;
		}
		
		if(correct){
			$(this).addClass('valid');
			$(this).parent().addClass('valid');
			if($(this).hasClass('control-phone') || $(this).hasClass('control-portable') || $(this).hasClass('control-number')){
				$(this).parent().parent().addClass('valid');
			}
			if($(this).hasClass('form-select')){
				$(this).parent().parent().addClass('valid');
			}
		}
		
		profileClubCheck($);
	});
	
	// Submit
	$('#step_infos').submit(function(event){
		event.preventDefault();
	});
}

function profileCheck($){
	var valid = true;
	
	if($('#email').hasClass('error') || $('#email').val() == '') valid = false;
	if($('#phone_number').hasClass('error') || $('#phone_number').val() == '') valid = false;
	if($('#profession').hasClass('error') || $('#profession').val() == '') valid = false;
	
	if($('#phone_home').is(':visible')){
		if($('#phone_number_home').hasClass('error') || $('#phone_number_home').val() == '') valid = false;
	}
	
	if($('#phone_work').is(':visible')){
		if($('#phone_number_work').hasClass('error') || $('#phone_number_work').val() == '') valid = false;
	}
	
	if($('#phone_home2').is(':visible')){
		if($('#phone_number_home2').hasClass('error') || $('#phone_number_home2').val() == '') valid = false;
	}
	
	$('#submit_grp input').attr('disabled', 'disabled');
	if(valid == true) {
		$('#submit_grp input').removeAttr('disabled');
	}
	
	return valid;
}

function profileClubCheck($){
	var valid = true;
	
	if($('#email').hasClass('error') || $('#email').val() == '') valid = false;
	if($('#phone_number').hasClass('error') || $('#phone_number').val() == '') valid = false;
	if($('#phone_number_work').hasClass('error') || $('#phone_number_work').val() == '') valid = false;
	
	$('#submit_grp_infos input').attr('disabled', 'disabled');
	if(valid == true) {
		$('#submit_grp_infos input').removeAttr('disabled');
	}
	
	return valid;
}

function profilePassword($){		
	// Contrôles
	$('input').keyup(function(){
		var id = $(this).attr('id');
		if(id == 'old_password' || id == 'new_password' || id == 'new_password_confirm'){		
			$(this).removeClass('error');
			$(this).removeClass('valid');
			$(this).parent().removeClass('error');
			$(this).parent().removeClass('valid');
			$(this).parent().parent().removeClass('error');
			$(this).parent().parent().removeClass('valid');
			
			var correct = true;
			
			if($(this).hasClass('required') && $(this).val()==''){
				$(this).addClass('error');
				$(this).parent().addClass('error');
				$(this).parent().parent().addClass('error');
				if($(this).hasClass('form-select')){
					$(this).parent().parent().addClass('error');
				}
				correct = false;
			}
			
			if($(this).hasClass('control-text') && $(this).val().length < 2 && $(this).val().length > 0){
				$(this).addClass('error');
				$(this).parent().addClass('error');
				correct = false;
			}
			
			$('.password_error').hide();
			if($(this).attr('id') == 'new_password_confirm' && $(this).val() != $('#new_password').val()){
				$(this).addClass('error');
				$(this).parent().addClass('error');
				correct = false;
				$('.password_error').show();
			}
			
			if(correct){
				$(this).addClass('valid');
				$(this).parent().addClass('valid');
				if($(this).hasClass('control-phone') || $(this).hasClass('control-portable') || $(this).hasClass('control-number')){
					$(this).parent().parent().addClass('valid');
				}
				if($(this).hasClass('form-select')){
					$(this).parent().parent().addClass('valid');
				}
				
				passwordCheck($);
			}
		}
	});
}

function passwordCheck($){
	var valid = true;
	
	if($('#old_password').hasClass('error') || $('#old_password').val() == '') valid = false;
	if($('#new_password').hasClass('error') || $('#new_password').val() == '') valid = false;
	if($('#new_password_confirm').hasClass('error') || $('#new_password_confirm').val() == '') valid = false;
	
	if($('#new_password').val() != $('#new_password_confirm').val()){
		valid = false;
	}
	
	$('#submit_grp input').attr('disabled', 'disabled');
	if(valid == true) {
		$('#submit_grp input').removeAttr('disabled');
	}
	
	return valid;
}

function initMenu($){
	$('#toggle_sidebar').click(function(){
		console.log('test');
		if(!sidebarClient && !sidebarClientHideClick){
			$( "#sidebar_client").slideDown();
			sidebarClient = true;
		}else{
			$( "#sidebar_client").slideUp();
			sidebarClient = false;
			sidebarClientHideClick = false;
		}
	});	
	
	$('a#close_sidebar_block').click(function(){
		$( "#sidebar_client" ).slideUp();
	});
	
	if($('#account_top_right').length){
		if($('body').width() > 1200){
			$('#account_top_right .segmented-control').addClass('medium_size');
			$('#account_top_right .segmented-control').removeClass('full_size');
		}else{
			$('#account_top_right .segmented-control').removeClass('medium_size');
			$('#account_top_right .segmented-control').addClass('full_size');
		}
	}
}

function initAccount($){
	if($('#account_top_right').length){
		if($('body').width() > 1200){
			$('#account_top_right .segmented-control').addClass('medium_size');
			$('#account_top_right .segmented-control').removeClass('full_size');
		}else{
			$('#account_top_right .segmented-control').removeClass('medium_size');
			$('#account_top_right .segmented-control').addClass('full_size');
		}
	}
}

var getParams = extractUrlParams();
var activity = false;
var manager = false;
var sidebarClient = false;
var sidebarClientHideClick = false;

jQuery(document).ready(function($){
	$('#sinature_buttons a').click(function(){
		$('#signature_popin').modal('hide');
	});
	
	$(document).mouseup(function (e){
		var container = $("#activity_block_container");
		if(container.has(e.target).length === 0 && activity){
			$("#activity_block").animate({width:"0px"}, 500, function(){activity = false;$("#activity_block").attr("style", "width:0px;overflow:hidden;");});
			$("#activity_block_container").width("0px");
		}
		
		var container = $("#header_manager");
		if(container.has(e.target).length === 0 && manager){
			//console.log(e.target);
			jQuery('#header_manager_menu').slideUp();
			jQuery('#header_manager').removeClass('open');
			manager = false;
		}
		
		var container = $("#sidebar_client");
		var button = $("#toggle_sidebar");
		if(container.has(e.target).length === 0 && button.has(e.target).length === 0 && jQuery('body').width() <= 768 && sidebarClient){
			container.hide();
			sidebarClient = false;
			sidebarClientHideClick = true;
			setTimeout(function(){sidebarClientHideClick = false;}, 500);
		}
	});
	
	$(window).on('resize', function(){
		if($('#account_top_right').length){
			if($('body').width() > 1200){
				$('#account_top_right .segmented-control').addClass('medium_size');
				$('#account_top_right .segmented-control').removeClass('full_size');
			}else{
				$('#account_top_right .segmented-control').removeClass('medium_size');
				$('#account_top_right .segmented-control').addClass('full_size');
			}
		}
	});
});