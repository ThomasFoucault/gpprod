app.directive('gpheader', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/struct/header.html?v=5'
		
	};
});

app.directive('gpactivities', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/struct/notifications.html'
		
	};
});

app.directive('gpmenu', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/struct/menu.html'
		
	};
});

app.directive('gpsynthesis', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/home.html'
		
	};
});

app.directive('gpaccount', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account.html?v=2'
		
	};
});

app.directive('gpdocuments', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/documents.html'
		
	};
});

app.directive('gpdocument', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/document.html'
		
	};
});

app.directive('gpprofile', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/profile/infos.html'
		
	};
});

app.directive('gppassword', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/profile/password.html'
		
	};
});

app.directive('gpfunds', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/funds.html'
		
	};
});

app.directive('gpsynthesisheader', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/home/synthesis.html?v=3'
		
	};
});

app.directive('gpsynthesisaccounts', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/home/accounts.html?v=3'
		
	};
});

app.directive('gpsynthesisrep', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/home/repartitions.html'
		
	};
});

/*app.directive('gpload', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/loading.html'
		
	};
});*/

app.directive('gpaccountheader', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/synthesis.html?v=3'
		
	};
});

app.directive('gpaccountperfs', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/performances.html?v=3'
		
	};
});

app.directive('gpaccountpositions', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/positions.html?v=2'
		
	};
});

app.directive('gpaccountpmvalues', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/pmvalues.html?v=2'
		
	};
});

app.directive('gpaccountmovements', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/movements.html'
		
	};
});

app.directive('gpaccountreps', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/repartitions.html'
		
	};
});

app.directive('gprib', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/account/rib.html'
		
	};
});

app.directive('clubsynthesis', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/synthesis.html?v=2'
		
	};
});

app.directive('clubclient', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/clients.html?v=6'
		
	};
});

app.directive('clubdocuments', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/documents.html'
		
	};
});

app.directive('clubprofile', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/profile.html'
		
	};
});

app.directive('clubpassword', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/password.html'
		
	};
});

app.directive('clubfunds', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/funds.html'
		
	};
});

app.directive('clubcontact', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/contact.html'
		
	};
});

app.directive('clubdocument', function(){
	return{
		restrict: 'E', //Element (Tag)
		templateUrl: 'views/club/document.html'
		
	};
});