var history_api = typeof history.pushState !== 'undefined';
if ( history_api ) history.pushState(null, '', '#Gestionprivee');

var app = angular.module('siteGPApp', ['ui.bootstrap', 'ngSanitize', 'ngCsv', 'ngCookies']);

app.run(function($rootScope, $location, $templateCache) {
	$rootScope.$on('$viewContentLoaded', function() {
		$templateCache.removeAll();
	});
   
	$rootScope.$on('$locationChangeStart', function(event, next, current){            
		// Here you can take the control and call your own functions:
		//alert('Sorry ! Back Button is disabled');
		// Prevent the browser default action (Going back):
		event.preventDefault();            
	});
});

app.config(['$tooltipProvider', function($tooltipProvider) {
	$tooltipProvider.options({animation: false});
}]);

/***************************************************
				Controlleurs GP
****************************************************/

app.controller('MainCtrl', function($rootScope, $scope, $sce, $http, $timeout, $cookies, $window, ApiServ, AccountServ) {
	$rootScope.history = [];
	$rootScope.historyBack = false;
	
	$rootScope.club = club;
	$rootScope.clubView = false;
	$rootScope.fundsURL = "https://www.lfde.com/les-fonds/?format=light";
	$rootScope.clientName = "";
	$rootScope.SignatureUrl = "";
	$rootScope.currentYear = new Date().getFullYear();
	$rootScope.lastYear = $rootScope.currentYear - 1;
	$scope.trustAsHtml = $sce.trustAsHtml;
	$scope.CGPLogo = ApiServ.getCGPLogo();
	
	if($rootScope.club){
		$rootScope.activeView = 'mangerSynthesis';
		$rootScope.history.push({
			view: 'mangerSynthesis',
			account: '',
			fid: '',
			pid: '',
			url: ''
		});
	}else{
		$rootScope.activeView = 'synthesis';
		$rootScope.history.push({
			view: 'synthesis',
			account: '',
			fid: '',
			pid: '',
			url: ''
		});
	}
	$rootScope.activeAccount = '';
	$rootScope.activeDocument = '';
	$rootScope.filter = 0;
	$rootScope.activeClientFid = '';
	$rootScope.activeClientPid = '';
	
	var login = ApiServ.getLogin();
	$rootScope.loginURL = ApiServ.getLoginURL();
	$rootScope.login = login.Salutation + " " + login.FirstName + " " + login.LastName;
	if(club){
		$rootScope.login = login.FirstName + " " + login.LastName;
		$rootScope.loginCGP = login.CGP;
	}
	$rootScope.message = login.Message;
	
	if(login.SignatureUrl != null){
		$rootScope.SignatureUrl = login.SignatureUrl;
		jQuery('#signature_popin').modal('show');
	}
	
	$scope.showManagerMenu = function(){
		if(jQuery('#header_manager').hasClass('open')){
			manager = false;
		}else{
			manager = true;
		}
		jQuery('#header_manager_menu').slideToggle();
		jQuery('#header_manager').toggleClass('open');
	};
	
	$rootScope.changeView = function(item){
		$rootScope.activeView = item;
		$rootScope.activeAccount = '';
		$rootScope.accounts = AccountServ.getMenuList($rootScope.activeAccount);
		jQuery('#sidebar_client_container li').removeClass('selected');
		jQuery('#sidebar_client_container li[rel="'+item+'"]').addClass('selected');
		if(item == 'password' || item == 'profile'){
			jQuery('#sidebar_client_container #sidebar_account_menu > li[rel="profile"]').addClass('selected');
		}
		
		if(club){
			$rootScope.clubView = true;
			if(item == 'mangerSynthesis' || item == 'mangerClients' || item == 'mangerDocuments' || item == 'mangerProfile' || item == 'mangerPassword' || item == 'mangerFunds' || item == 'mangerContact'){
				$rootScope.clubView = false;
			}
		}
		
		if(jQuery('body').width() <= 768) jQuery("#sidebar_client").hide();
		
		$rootScope.history.push({
			view: item,
			account: '',
			fid: $rootScope.activeClientFid,
			pid: $rootScope.activeClientPid,
			url: ''
		});
	};
	
	$rootScope.showHome = function(){
		if(club){
			$rootScope.changeView('mangerSynthesis');
		}else{
			$rootScope.changeView('synthesis');
		}
	};
	
	$rootScope.changeAccount = function(account_id){
		$rootScope.activeView = 'loading';
		$rootScope.activeAccount = account_id;
		$timeout(function() {
            $rootScope.activeView = 'account';
			jQuery('#sidebar_client_container li').removeClass('selected');
			$rootScope.accounts = AccountServ.getMenuList($rootScope.activeAccount);
			breadcrump(jQuery);
        }, 1);
		
		if(jQuery('body').width() <= 768) jQuery("#sidebar_client").hide();
		
		$rootScope.history.push({
			view: 'account',
			account: account_id,
			fid: $rootScope.activeClientFid,
			pid: $rootScope.activeClientPid,
			url: ''
		});
	};
	
	$rootScope.showFunds = function(url){
		if(url != ''){
			$rootScope.fundsURL = url + "?format=light";
		}else{
			$rootScope.fundsURL = "https://www.lfde.com/les-fonds/?format=light";
		}
		
		$rootScope.activeView = 'funds';
		$rootScope.activeAccount = '';
		$rootScope.accounts = AccountServ.getMenuList($rootScope.activeAccount);
		jQuery('#sidebar_client_container li').removeClass('selected');
		jQuery('#sidebar_client_container li[rel="funds"]').addClass('selected');
		$rootScope.clubView = false;
		
		$timeout(function() {
            jQuery('#funds_iframe').attr('src', $rootScope.fundsURL);
        }, 1);
		
		if(jQuery('body').width() <= 768) jQuery("#sidebar_client").hide();
		
		$rootScope.history.push({
			view: 'funds',
			account: '',
			fid: $rootScope.activeClientFid,
			pid: $rootScope.activeClientPid,
			url: $rootScope.fundsURL
		});
	};
	
	$rootScope.showMobileMenu = function(){
		if(!sidebarClient){
			jQuery("#sidebar_client").show();
			sidebarClient = true;
		}else{
			jQuery("#sidebar_client").hide();
			sidebarClient = false;
		}
	};
	
	$rootScope.showClient = function(fid, pid){
		$rootScope.activeClientFid = fid;
		$rootScope.activeClientPid = pid;
		
		$rootScope.activeView = 'synthesis';
		jQuery('#sidebar_client_container li').removeClass('selected');
		$rootScope.clubView = true;
		
		ApiServ.initClientInfos(fid, pid);
		
		$rootScope.clientName = ApiServ.getClientName();
		
		$rootScope.history.push({
			view: 'synthesis',
			account: '',
			fid: $rootScope.activeClientFid,
			pid: $rootScope.activeClientPid,
			url: ''
		});
	};
	
	$rootScope.goBack = function(){
		if(!$rootScope.historyBack && $rootScope.history.length > 1){
			$rootScope.historyBack = true;
			$rootScope.history.pop();
			var backPage = $rootScope.history[$rootScope.history.length -1];
			
			$timeout(function(){
				if(backPage){
					switch(backPage.view){
						case 'account':
							$rootScope.activeView = 'loading';
							$rootScope.activeAccount = backPage.account;
							$timeout(function() {
								$rootScope.activeView = 'account';
								jQuery('#sidebar_client_container li').removeClass('selected');
								$rootScope.accounts = AccountServ.getMenuList($rootScope.activeAccount);
								breadcrump(jQuery);
							}, 1);
							break;
						case 'funds':
							$rootScope.activeView = 'funds';
							$rootScope.activeAccount = '';
							$rootScope.accounts = AccountServ.getMenuList($rootScope.activeAccount);
							jQuery('#sidebar_client_container li').removeClass('selected');
							jQuery('#sidebar_client_container li[rel="funds"]').addClass('selected');
							$rootScope.clubView = false;
							
							$timeout(function() {
								jQuery('#funds_iframe').attr('src', backPage.url);
							}, 1);
							break;
						default:
							$rootScope.activeView = backPage.view;
							$rootScope.activeAccount = '';
							$rootScope.accounts = AccountServ.getMenuList($rootScope.activeAccount);
							jQuery('#sidebar_client_container li').removeClass('selected');
							jQuery('#sidebar_client_container li[rel="'+backPage.view+'"]').addClass('selected');
							
							if(backPage.view == 'synthesis' && club && $rootScope.activeClientFid != backPage.fid){
								// Cas où on affiche un client autre que celui déjà affiché
								$rootScope.activeClientFid = backPage.fid;
								$rootScope.activeClientPid = backPage.pid;
								
								$rootScope.activeView = 'synthesis';
								jQuery('#sidebar_client_container li').removeClass('selected');
								$rootScope.clubView = true;
								
								ApiServ.initClientInfos(fid, pid);
								
								$rootScope.clientName = ApiServ.getClientName();
							}else{				
								if(backPage.view == 'password' || backPage.view == 'profile'){
									jQuery('#sidebar_client_container #sidebar_account_menu > li[rel="profile"]').addClass('selected');
								}
								
								if(club){
									$rootScope.clubView = true;
									if(backPage.view == 'mangerSynthesis' || backPage.view == 'mangerClients' || backPage.view == 'mangerDocuments' || backPage.view == 'mangerProfile' || backPage.view == 'mangerPassword' || backPage.view == 'mangerFunds' || backPage.view == 'mangerContact'){
										$rootScope.clubView = false;
									}
								}
							}
					}
					
					if(jQuery('body').width() <= 768) jQuery("#sidebar_client").hide();
				}/*else{
					window.location.href = $rootScope.loginURL;
				}*/
				$rootScope.historyBack = false;
			}, 1);
		}
	}
	
	$rootScope.disconnection = function(){
		$cookies['deadToken'] = getParams['at'];
		ApiServ.disconnect();
	};
	
	$window.onbeforeunload =  $rootScope.disconnection;
	
	jQuery(window).on('popstate', function(e) {
		$rootScope.goBack();
	});
});

app.controller('MenuCtrl', function($rootScope, $scope, $timeout, AccountServ, ApiServ) {
	$rootScope.accounts = AccountServ.getMenuList();
	$scope.manager = ApiServ.getManager();
	
	$scope.showManager = function(){
		jQuery('#manager_popin').modal('show');
		jQuery('#manager_popin .close_manager').click(function(){jQuery('#manager_popin').modal('hide');});
	};
});

app.controller('ActivitiesCtrl', function($rootScope, $scope, $sce, ActivityServ, ApiServ) {
	$scope.activities = ActivityServ.getActivities();
	$scope.unreadClass = 'read';
	$scope.unreadTotal = '';
	
	if($scope.activities.recents.length > 0){
		$scope.unreadClass = 'unread';
		$scope.unreadTotal = $scope.activities.recents.length;
	}
	
	$scope.showActivities = function(){
		showActivitiesBlock(jQuery);
		ApiServ.readActivities();
	}
});

app.controller('HomeCtrl', function($rootScope, $scope, $sce, $timeout, AccountServ, FormatServ, GraphServ, PdfServ, RepartitionServ) {
	$scope.accounts = AccountServ.getHomeList($rootScope.filter);
	$scope.trustAsHtml = $sce.trustAsHtml;
	$scope.perfNegative = '';
	$scope.datePosition = AccountServ.getPositionDate();
	
	$scope.predicate = 'description';
	$scope.reverse = false;
	
	$scope.order = function(predicate) {
		$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
		$scope.predicate = predicate;
	};
	
	$timeout(function() {
		GraphServ.load_data($rootScope.filter, $scope.accounts, false, "histo_positions", defaultGraph);
	}, 1000);
	
	$scope.totalPositions = function(brut){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.amountBrut;
		}
		if(brut)
			return FormatServ.amountNoStyle(total, 0, ',', ' ', '€');
		else
			return FormatServ.amount(total, 0, ',', ' ', '€');
	}
	$scope.totalPerf = function(brut){
		$scope.perfNegative = '';
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.perfEuroBrut;
		}
		if(total < 0) $scope.perfNegative = 'negative';
		if(brut)
			return total;
		else
			return FormatServ.amount(total, 0, ',', ' ', '€');
	}
	$scope.totalAR = function(brut){
		 var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.apportsRetraitsBrut;
		}
		if(brut)
			return FormatServ.amountNoStyle(total, 0, ',', ' ', '€');
		else
			return FormatServ.amount(total, 0, ',', ' ', '€');
	}
	$scope.totalFrais = function(brut){
		 var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.fraisBrut;
		}
		if(brut)
			return FormatServ.amountNoStyle(total, 0, ',', ' ', '€');
		else
			return FormatServ.amount(total, 0, ',', ' ', '€');
	}
	
	$scope.actif1Value = function(){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif1Value;
		}
		return total;
	}
	$scope.actif2Value = function(){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif2Value;
		}
		return total;
	}
	$scope.actif3Value = function(){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif3Value;
		}
		return total;
	}
	$scope.actif4Value = function(){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif4Value;
		}
		return total;
	}
	$scope.actif5Value = function(){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif5Value;
		}
		return total;
	}
	
	$scope.totalActif = $scope.actif1Value() + $scope.actif2Value() + $scope.actif3Value() + $scope.actif4Value() + $scope.actif5Value();
	
	$scope.actif1RealPercent = function(brut){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif1Value;
		}
		total = (total * 100) / $scope.totalActif;
		if(brut)
			return FormatServ.amountNoStyle(total, 2, ',', ' ', '%');
		else
			return FormatServ.amount(total, 2, ',', ' ', '%');
	}
	$scope.actif2RealPercent = function(brut){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif2Value;
		}
		total = (total * 100) / $scope.totalActif;
		if(brut)
			return FormatServ.amountNoStyle(total, 2, ',', ' ', '%');
		else
			return FormatServ.amount(total, 2, ',', ' ', '%');
	}
	$scope.actif3RealPercent = function(brut){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif3Value;
		}
		total = (total * 100) / $scope.totalActif;
		if(brut)
			return FormatServ.amountNoStyle(total, 2, ',', ' ', '%');
		else
			return FormatServ.amount(total, 2, ',', ' ', '%');
	}
	$scope.actif4RealPercent = function(brut){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif4Value;
		}
		total = (total * 100) / $scope.totalActif;
		if(brut)
			return FormatServ.amountNoStyle(total, 2, ',', ' ', '%');
		else
			return FormatServ.amount(total, 2, ',', ' ', '%');
	}
	$scope.actif5RealPercent = function(brut){
		var total = 0;
		for(var i = 0; i < $scope.accounts.length; i++){
			var account = $scope.accounts[i];
			if(account.status != 'inactive') total += account.actif5Value;
		}
		total = (total * 100) / $scope.totalActif;
		if(brut)
			return FormatServ.amountNoStyle(total, 2, ',', ' ', '%');
		else
			return FormatServ.amount(total, 2, ',', ' ', '%');
	}
	
	$scope.supportRepartitions = RepartitionServ.getAccountsSupportRepartition($scope.accounts, true);
	
	var maxActif = 0;
	if($scope.actif1Value() > maxActif) maxActif = $scope.actif1Value();
	if($scope.actif2Value() > maxActif) maxActif = $scope.actif2Value();
	if($scope.actif3Value() > maxActif) maxActif = $scope.actif3Value();
	if($scope.actif4Value() > maxActif) maxActif = $scope.actif4Value();
	if($scope.actif5Value() > maxActif) maxActif = $scope.actif5Value();
	
	$scope.actif1BarPercent = ($scope.actif1Value() * 100) / maxActif;
	$scope.actif2BarPercent = ($scope.actif2Value() * 100) / maxActif;
	$scope.actif3BarPercent = ($scope.actif3Value() * 100) / maxActif;
	$scope.actif4BarPercent = ($scope.actif4Value() * 100) / maxActif;
	$scope.actif5BarPercent = ($scope.actif5Value() * 100) / maxActif;
	
	$scope.changeFilter = function(value){
		$rootScope.filter = value;
		$scope.accounts = AccountServ.getHomeList($rootScope.filter);
		GraphServ.load_data($rootScope.filter, $scope.accounts, false, "histo_positions", defaultGraph);
	};
	
	$scope.toggleAccount = function(account_id){
		for(var i = 0; i < $scope.accounts.length; i++){
			if($scope.accounts[i].num == account_id){
				if($scope.accounts[i].status == 'inactive'){
					$scope.accounts[i].status = '';
					$scope.accounts[i].buttonLabel = 'Masquer';
				}else{
					$scope.accounts[i].status = 'inactive';
					$scope.accounts[i].buttonLabel = 'Afficher';
				}
			}
		}
		GraphServ.load_data($rootScope.filter, $scope.accounts, false, "histo_positions", defaultGraph);
		
		$scope.totalActif = $scope.actif1Value() + $scope.actif2Value() + $scope.actif3Value() + $scope.actif4Value() + $scope.actif5Value();
		$scope.supportRepartitions = RepartitionServ.getAccountsSupportRepartition($scope.accounts, true);
		
		var maxActif = 0;
		if($scope.actif1Value() > maxActif) maxActif = $scope.actif1Value();
		if($scope.actif2Value() > maxActif) maxActif = $scope.actif2Value();
		if($scope.actif3Value() > maxActif) maxActif = $scope.actif3Value();
		if($scope.actif4Value() > maxActif) maxActif = $scope.actif4Value();
		if($scope.actif5Value() > maxActif) maxActif = $scope.actif5Value();
		
		$scope.actif1BarPercent = ($scope.actif1Value() * 100) / maxActif;
		$scope.actif2BarPercent = ($scope.actif2Value() * 100) / maxActif;
		$scope.actif3BarPercent = ($scope.actif3Value() * 100) / maxActif;
		$scope.actif4BarPercent = ($scope.actif4Value() * 100) / maxActif;
		$scope.actif5BarPercent = ($scope.actif5Value() * 100) / maxActif;
	}
	
	$scope.exportToPDF = function(){
		jQuery('#waiting_popin').modal('show');
		PdfServ.exportClientHome($scope.accounts, $rootScope.filter, $scope.totalPositions(true), $scope.totalPerf(true), $scope.totalAR(true), $scope.totalFrais(true), $scope.actif1RealPercent(true), $scope.actif2RealPercent(true), $scope.actif3RealPercent(true), $scope.actif4RealPercent(true), $scope.actif5RealPercent(true), $scope.actif1BarPercent, $scope.actif2BarPercent, $scope.actif3BarPercent, $scope.actif4BarPercent, $scope.actif5BarPercent, club);
		GraphServ.load_data($rootScope.filter, $scope.accounts, false, "histo_positions", defaultGraph);		
	};
});

app.controller('AccountCtrl', function($rootScope, $scope, $sce, AccountServ, FormatServ, GraphServ, PositionServ, PMValuesServ, MovementServ, RepartitionServ, PdfServ) {
	$scope.account = AccountServ.getAccount($rootScope.activeAccount, $rootScope.filter);
	$scope.trustAsHtml = $sce.trustAsHtml;
	
	$scope.num = $scope.account.num;
	$scope.owner = $scope.account.owner;
	$scope.type = $scope.account.type;
	$scope.typeShort = $scope.account.typeShort;
	$scope.titulaire = $scope.account.titulaire;
	$scope.accountNumber = $scope.account.num;
	$scope.datePosition = AccountServ.getPositionDate();
	$scope.amountMenu = $scope.account.amountMenu;
	$scope.amountPositive = $scope.account.amountPositive;
	
	$scope.accountPosition = $scope.account.amount;
	$scope.accountPerf = $scope.account.perfEuro;
	$scope.accountPerfPercent = $scope.account.perfPercent;
	$scope.accountAR = $scope.account.apportsRetraits;
	$scope.accountFrais = $scope.account.frais;
	$scope.accountOpenDate = $scope.account.dateOpen;
	$scope.accountApports = $scope.account.apports;
	$scope.accountVersements = $scope.account.versements;
	$scope.perfPositive = $scope.account.perfPositive;
	
	$scope.actif1RealPercent = $scope.account.actif1RealPercent;
	$scope.actif1BarPercent = $scope.account.actif1BarPercent;
	$scope.actif2RealPercent = $scope.account.actif2RealPercent;
	$scope.actif2BarPercent = $scope.account.actif2BarPercent;
	$scope.actif3RealPercent = $scope.account.actif3RealPercent;
	$scope.actif3BarPercent = $scope.account.actif3BarPercent;
	$scope.actif4RealPercent = $scope.account.actif4RealPercent;
	$scope.actif4BarPercent = $scope.account.actif4BarPercent;
	$scope.actif5RealPercent = $scope.account.actif5RealPercent;
	$scope.actif5BarPercent = $scope.account.actif5BarPercent;
	
	$scope.positions = PositionServ.getPositions($scope.account);
	$scope.positionsExport = PositionServ.getPositionsForExport($scope.account);
	$scope.positionsList = $scope.positions.positions;
	$scope.movements = MovementServ.getMovements($scope.account);
	$scope.movementsExport = MovementServ.getMovementsForExport($scope.account);
	$scope.pmvalues = PMValuesServ.getValues($scope.account);
	$scope.pmvaluesExport = PMValuesServ.getValuesForExport($scope.account);
	
	$scope.filenamePositions = "Compte-" + $scope.num + "-Positions-" + $scope.datePosition.replace(/ /g, '-') + ".csv";
	$scope.filenameMovements = "Compte-" + $scope.num + "-Mouvements-" + $scope.datePosition.replace(/ /g, '-') + ".csv";
	$scope.filenamePMValues = "Compte-" + $scope.num + "-PlusouMoinsValues-" + $scope.datePosition.replace(/ /g, '-') + ".csv";
	
	$scope.positionsPredicate = 'name';
	$scope.positionsReverse = false;
	
	$scope.orderPositions = function(predicate) {
		$scope.positionsReverse = ($scope.positionsPredicate === predicate) ? !$scope.positionsReverse : false;
		$scope.positionsPredicate = predicate;
	};
	
	$scope.movementsPredicate = 'dateTime';
	$scope.movementsReverse = true;
	
	$scope.orderMovements = function(predicate) {
		$scope.movementsReverse = ($scope.movementsPredicate === predicate) ? !$scope.movementsReverse : false;
		$scope.movementsPredicate = predicate;
	};
	
	$scope.valuesPredicate = 'isin';
	$scope.valuesReverse = false;
	
	$scope.orderValues = function(predicate) {
		$scope.valuesReverse = ($scope.valuesPredicate === predicate) ? !$scope.valuesReverse : false;
		$scope.valuesPredicate = predicate;
	};
	
	$scope.supportRepartitions = RepartitionServ.getAccountSupportRepartition($scope.account);
	
	setTimeout(function(){
		var accounts = [];
		accounts.push($scope.account);
		GraphServ.load_data($rootScope.filter, accounts, true, "histo_positions", defaultGraph);
	}, 500);
	
	$scope.changeFilter = function(value){
		$rootScope.filter = value;
		$scope.account = AccountServ.getAccount($rootScope.activeAccount, $rootScope.filter);
		
		$scope.accountPosition = $scope.account.amount;
		$scope.accountPerf = $scope.account.perfEuro;
		$scope.accountPerfPercent = $scope.account.perfPercent;
		$scope.accountAR = $scope.account.apportsRetraits;
		$scope.accountFrais = $scope.account.frais;
		$scope.accountOpenDate = $scope.account.dateOpen;
		$scope.accountApports = $scope.account.apports;
		$scope.accountVersements = $scope.account.versements;
		$scope.perfPositive = $scope.account.perfPositive;
		
		var accounts = [];
		accounts.push($scope.account);
		GraphServ.load_data($rootScope.filter, accounts, true, "histo_positions", defaultGraph);
	};
	
	$scope.downloadPositions = function(){
		var data = PositionServ.getPositionsForExport($scope.account);
		var csvContent = "data:text/csv;charset=utf-8,";
		data.forEach(function(infoArray, index){
			dataString = infoArray.join(";");
			csvContent += index < data.length ? dataString+ "\n" : dataString;
		}); 
		
		var encodedUri = encodeURI(csvContent);
		window.open(encodedUri);
	};
	
	$scope.showRIB = function(account_id){
		var docDefinition = PdfServ.getRIBconfig($scope.account);
		
		pdfMake.createPdf(docDefinition).download('RIB '+$scope.account.type+' n°'+$scope.account.num+'.pdf');
	};
	
	initAccount(jQuery);
});

app.controller('RibCtrl', function($rootScope, $scope, AccountServ) {
	$scope.account = AccountServ.getAccount($rootScope.activeAccount, $rootScope.filter);
	
	$scope.id = $scope.account.id;
	$scope.num = $scope.account.num;
	$scope.owner = $scope.account.titulaire;
	$scope.type = $scope.account.type;
	$scope.iban = $scope.account.iban;
	$scope.bic = $scope.account.bic;
});

app.controller('DocumentsCtrl', function($rootScope, $scope, $http, $timeout, $sce, DocumentServ, PdfServ, ApiServ) {
	$scope.docCategories = DocumentServ.getDocuments();
	$scope.accounts = DocumentServ.getAccounts();
	$scope.showRibs = ($scope.accounts.length > 0);
	$scope.trustAsHtml = $sce.trustAsHtml;
	
	$scope.loadDocument = function(document_id){
		$http({
			method: 'GET',
			url: urlAPI + 'api/Documents/' + document_id,
			responseType: 'arraybuffer',
			headers: {
				'Accept':'text/html,application/xhtml+xml,application/xml;',
				'Content-Type': 'application/json; charset=utf-8',
				//'Host':'login.fin-echiquier.fr',
				//'Accept-Encoding':'gzip',
				'Authorization':'Bearer ' + ApiServ.getAccessToken(),
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET, POST',
				'Access-Control-Allow-Headers': '*'
			}
		}).success(function (response) {
			console.log(response);
			var file = new Blob([response], {type: 'application/pdf'});
			var fileURL = URL.createObjectURL(file);
			
			window.open(fileURL);
		}).error(function () {
			if(club){
				window.location.href = loginClubURL;
			}else{
				window.location.href = loginURL;
			}
			return false;
		});
		/*$rootScope.activeView = 'loading';
		$rootScope.activeDocument = document_id;
		$timeout(function() {
            $rootScope.activeView = 'document';
        }, 1);*/
	};
	
	$scope.showRIB = function(account){
		var docDefinition = PdfServ.getRIBconfig(account);
		
		pdfMake.createPdf(docDefinition).open('RIB '+account.type+' n°'+account.num+'.pdf');
	};
});

app.controller('DocumentCtrl', function($rootScope, $scope, $http, $sce, ApiServ) {
	$http({
		method: 'GET',
		url: urlAPI + 'api/Documents/' + $rootScope.activeDocument,
		responseType: 'arraybuffer',
		headers: {
			'Accept':'text/html,application/xhtml+xml,application/xml;',
			'Content-Type': 'application/json; charset=utf-8',
			//'Host':'login.fin-echiquier.fr',
			//'Accept-Encoding':'gzip',
			'Authorization':'Bearer ' + ApiServ.getAccessToken(),
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Methods': 'GET, POST',
			'Access-Control-Allow-Headers': '*'
		}
	}).success(function (response) {
		console.log(response);
		var file = new Blob([response], {type: 'application/pdf'});
		var fileURL = URL.createObjectURL(file);
		
		$scope.content = $sce.trustAsResourceUrl(fileURL);
	}).error(function () {
		if(club){
			window.location.href = loginClubURL;
		}else{
			window.location.href = loginURL;
		}
		return false;
	});
});

app.controller('PasswordCtrl', function($rootScope, $scope, $timeout, $sce, ApiServ) {
	$scope.changed = ApiServ.changed;
	$scope.success = ApiServ.success;
	
	profilePassword(jQuery);
	
	var init = function(){
		jQuery('#step_password').submit(function(event){
			event.preventDefault();
			if(passwordCheck(jQuery)){
				ApiServ.resetPassword(jQuery('#old_password').val(), jQuery('#new_password').val());
			}
		});
	};
	
	init();
});

app.controller('InfosCtrl', function($rootScope, $scope, $timeout, $sce, ApiServ, ProfileServ) {
	$scope.changed = false;
	$scope.success = false;
	
	$scope.infos = ProfileServ.getProfileInfos(false);
	
	profileInformations(jQuery, $scope.infos.minors, $scope.infos.majors, club);
	
	$scope.submit = function(){
		jQuery('#submit_grp_infos input').addClass('waiting').val('Enregistrement en cours...');
		jQuery('#infos_success, #infos_failed').hide();
		ApiServ.changeInfos(jQuery);
	};
});

/***************************************************
				Controlleurs CGP
****************************************************/

app.controller('ClientsCtrl', function($rootScope, $scope, $sce, ApiServ, ClientsServ) {
	$scope.search = '';
	
	$scope.clients = ClientsServ.getClients($rootScope.filter, $scope.search);
	$scope.trustAsHtml = $sce.trustAsHtml;
	
	$scope.predicate = 'name';
	$scope.reverse = false;
	
	$scope.keypressFilterControll = function(e){
		if(e.keyCode == 13){
			$scope.searchClient();
		}
	};
	
	$scope.order = function(predicate) {
		$scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
		$scope.predicate = predicate;
	};
	
	$scope.searchClient = function(){
		$scope.search = $('input#search_client').val();
		$scope.clients = ClientsServ.getClients($rootScope.filter, $scope.search);
	};
	
	$scope.changeFilter = function(value){
		$rootScope.filter = value;
		$scope.clients = ClientsServ.getClients($rootScope.filter, $scope.search);
	};
});

app.controller('ClubSynthesisCtrl', function($rootScope, $scope, $sce, $timeout, ApiServ, ClubSynthServ, GraphServ) {
	$scope.synthesis = ClubSynthServ.getSynthesisData();
	$scope.trustAsHtml = $sce.trustAsHtml;
	
	$scope.showEncoursDetails = function(level){
		jQuery('.synthesis_details.level_'+level).toggle();
		jQuery('#see_plus_'+level).toggleClass('menos');
		if(level == 1 && !jQuery('.synthesis_details.level_'+level).is(':visible')){
			jQuery('.synthesis_details.level_2').hide();
			jQuery('#see_plus_2').removeClass('menos');
		}
	};
	
	$timeout(function() {
		GraphServ.initPie($scope.synthesis);
	}, 500);
});

app.controller('DocumentsClubCtrl', function($rootScope, $scope, $timeout, $sce, DocumentClubServ, PdfServ, $http, ApiServ) {
	$scope.docCategories = DocumentClubServ.getDocuments();
	$scope.trustAsHtml = $sce.trustAsHtml;
	
	$scope.loadDocument = function(document_id){
		$http({
			method: 'GET',
			url: urlAPI + 'api/Documents/' + document_id,
			responseType: 'arraybuffer',
			headers: {
				'Accept':'text/html,application/xhtml+xml,application/xml;',
				'Content-Type': 'application/json; charset=utf-8',
				//'Host':'login.fin-echiquier.fr',
				//'Accept-Encoding':'gzip',
				'Authorization':'Bearer ' + ApiServ.getAccessToken(),
				'Access-Control-Allow-Origin': '*',
				'Access-Control-Allow-Methods': 'GET, POST',
				'Access-Control-Allow-Headers': '*'
			}
		}).success(function (response) {
			console.log(response);
			var file = new Blob([response], {type: 'application/pdf'});
			var fileURL = URL.createObjectURL(file);
			
			window.open(fileURL);
		}).error(function () {
			if(club){
				window.location.href = loginClubURL;
			}else{
				window.location.href = loginURL;
			}
			return false;
		});
	};
});

app.controller('DocumentClubCtrl', function($rootScope, $scope, $http, $sce, ApiServ) {
	$http({
		method: 'GET',
		url: urlAPI + 'api/Documents/' + $rootScope.activeDocument,
		responseType: 'arraybuffer',
		headers: {
			'Accept':'text/html,application/xhtml+xml,application/xml;',
			'Content-Type': 'application/json; charset=utf-8',
			//'Host':'login.fin-echiquier.fr',
			//'Accept-Encoding':'gzip',
			'Authorization':'Bearer ' + ApiServ.getAccessToken(),
			'Access-Control-Allow-Origin': '*',
			'Access-Control-Allow-Methods': 'GET, POST',
			'Access-Control-Allow-Headers': '*'
		}
	}).success(function (response) {
		console.log(response);
		var file = new Blob([response], {type: 'application/pdf'});
		var fileURL = URL.createObjectURL(file);
		
		$scope.content = $sce.trustAsResourceUrl(fileURL);
	}).error(function () {
		if(club){
			window.location.href = loginClubURL;
		}else{
			window.location.href = loginURL;
		}
		return false;
	});
});

app.controller('ContactClubCtrl', function($rootScope, $scope, ApiServ) {
	$scope.contact = ApiServ.getContact();
	$scope.assistante = $scope.contact.Assistante;
	$scope.rc = $scope.contact.ResponsableCommercial;
});

app.controller('InfosClubCtrl', function($rootScope, $scope, $timeout, $sce, ApiServ, ProfileServ) {
	// Infos
	$scope.changed = false;
	$scope.success = false;
	
	$scope.infos = ProfileServ.getProfileInfos(true);
	
	profileClubInformations(jQuery, $scope.infos.minors, $scope.infos.majors, club);
	
	$scope.submit = function(){
		jQuery('#submit_grp_infos input').addClass('waiting').val('Enregistrement en cours...');
		jQuery('#infos_success, #infos_failed').hide();
		ApiServ.changeClubInfos(jQuery);
	};
});

app.controller('PasswordClubCtrl', function($rootScope, $scope, $timeout, $sce, ApiServ, ProfileServ) {
	// Mot de passe
	$scope.changed = ApiServ.changed;
	$scope.success = ApiServ.success;
	
	profilePassword(jQuery);
	
	var init = function(){
		jQuery('#step_password').submit(function(event){
			event.preventDefault();
			if(passwordCheck(jQuery)){
				ApiServ.resetClubPassword(jQuery('#old_password').val(), jQuery('#new_password').val());
			}
		});
	};
	
	init();
});

